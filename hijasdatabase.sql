/*
Navicat MySQL Data Transfer

Source Server         : WAMP
Source Server Version : 50612
Source Host           : 127.0.0.1:3306
Source Database       : hijasdatabase

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2015-03-03 16:08:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fees
-- ----------------------------
DROP TABLE IF EXISTS `fees`;
CREATE TABLE `fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(225) NOT NULL,
  `grade` int(2) NOT NULL,
  `fee` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fees
-- ----------------------------
INSERT INTO `fees` VALUES ('2', 'Year Book', '12', '500');
INSERT INTO `fees` VALUES ('3', 'Student Hand Book', '12', '100');
INSERT INTO `fees` VALUES ('4', 'Parent Teachers Association', '12', '100');

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student` int(255) NOT NULL,
  `schoolyear` varchar(9) NOT NULL,
  `amount` varchar(250) NOT NULL,
  `date` varchar(32) NOT NULL,
  `method` varchar(255) NOT NULL,
  `receipt` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payments
-- ----------------------------
INSERT INTO `payments` VALUES ('1', '10', '2014-2015', '100.00', 'March 1 2015, 3:07 am', 'cash', '32412523521212');
INSERT INTO `payments` VALUES ('2', '10', '2014-2015', '3,000.50', 'March 1 2015, 7:02 am', 'cash', '34233345846245');

-- ----------------------------
-- Table structure for schoolyear
-- ----------------------------
DROP TABLE IF EXISTS `schoolyear`;
CREATE TABLE `schoolyear` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of schoolyear
-- ----------------------------
INSERT INTO `schoolyear` VALUES ('13', '2014-2015');

-- ----------------------------
-- Table structure for sections
-- ----------------------------
DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections` (
  `sectionid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1999) NOT NULL,
  `level` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `students` longtext NOT NULL,
  PRIMARY KEY (`sectionid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sections
-- ----------------------------
INSERT INTO `sections` VALUES ('9', '3rd Year A', '11', '8', '');
INSERT INTO `sections` VALUES ('8', '4th Year A', '12', '7', '');
INSERT INTO `sections` VALUES ('7', '4th Year B', '12', '6', '');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT,
  `studentid` varchar(32) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `section` varchar(32) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `level` int(11) NOT NULL,
  `subjects` longtext NOT NULL,
  `character` longtext NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES ('8', '0125', 'Shiela Mae', 'Cerbas', 'L', '7', '', '12', '', '', '');
INSERT INTO `students` VALUES ('12', '0126', 'Marites', 'Esmante', 'G', '7', '', '12', '', '', '');
INSERT INTO `students` VALUES ('11', '0124', 'Judy Ann', 'Adonis', 'B', '7', '', '12', '', '', '');
INSERT INTO `students` VALUES ('6', '0123', 'Roulegrave', 'Gamboa', 'S', '7', '09464575779', '12', '', '', '');
INSERT INTO `students` VALUES ('10', '0127', 'Ernalyn', 'Espada', 'A', '7', '', '12', '', '', '');
INSERT INTO `students` VALUES ('13', '0111', 'Mark Louie', 'ParreÃ±o', 'Y', '8', '651651651', '12', '', '1000', '10000');
INSERT INTO `students` VALUES ('14', '0112', 'Shiela Mae', 'Rendaje', 'A', '8', '', '12', '', '', '');
INSERT INTO `students` VALUES ('15', '0113', 'Sharmaine Anne', 'Rendaje', 'A', '8', '', '12', '', '', '');
INSERT INTO `students` VALUES ('16', '0114', 'Rhenly', 'Fango', 'G', '8', '', '12', '', '', '');
INSERT INTO `students` VALUES ('17', '0115', 'Jason', 'Porras', 'L', '8', '', '12', '', '', '');
INSERT INTO `students` VALUES ('18', '3210', 'Jeren', 'Pastor', 'V', '9', '09093323605', '11', '', '', '');

-- ----------------------------
-- Table structure for subjects
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(32) NOT NULL,
  `student` varchar(32) NOT NULL,
  `teacher` varchar(32) NOT NULL,
  `schoolyear` varchar(32) NOT NULL,
  `1st` varchar(32) NOT NULL,
  `2nd` varchar(32) NOT NULL,
  `3rd` varchar(32) NOT NULL,
  `4th` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subjects
-- ----------------------------
INSERT INTO `subjects` VALUES ('18', 'Science', '13', '7', '2014-2015', '75', '75', '75', '75');
INSERT INTO `subjects` VALUES ('12', 'Math', '11', '6', '2014-2015', '80', '85', '88', '85');
INSERT INTO `subjects` VALUES ('13', 'Math', '8', '6', '2014-2015', '87', '88', '85', '84');
INSERT INTO `subjects` VALUES ('14', 'Math', '12', '6', '2014-2015', '86', '84', '85', '88');
INSERT INTO `subjects` VALUES ('15', 'Math', '10', '6', '2014-2015', '85', '85', '84', '86');
INSERT INTO `subjects` VALUES ('16', 'Math', '6', '6', '2014-2015', '88', '85', '86', '84');
INSERT INTO `subjects` VALUES ('17', 'Science', '17', '7', '2014-2015', '85', '85', '84', '87');
INSERT INTO `subjects` VALUES ('19', 'Science', '16', '7', '2014-2015', '85', '85', '85', '85');
INSERT INTO `subjects` VALUES ('20', 'Science', '14', '7', '2014-2015', '86', '84', '85', '85');
INSERT INTO `subjects` VALUES ('21', 'Science', '15', '7', '2014-2015', '86', '84', '84', '85');
INSERT INTO `subjects` VALUES ('22', 'Computer', '18', '8', '2014-2015', '99', '85', '75', '65');

-- ----------------------------
-- Table structure for tuitions
-- ----------------------------
DROP TABLE IF EXISTS `tuitions`;
CREATE TABLE `tuitions` (
  `grade` int(225) NOT NULL,
  `tuitionfee` varchar(225) NOT NULL,
  PRIMARY KEY (`grade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tuitions
-- ----------------------------
INSERT INTO `tuitions` VALUES ('1', '10,000.00');
INSERT INTO `tuitions` VALUES ('2', '10,000.00');
INSERT INTO `tuitions` VALUES ('3', '10,000.00');
INSERT INTO `tuitions` VALUES ('4', '10,000.00');
INSERT INTO `tuitions` VALUES ('5', '10,000.00');
INSERT INTO `tuitions` VALUES ('6', '10,000.00');
INSERT INTO `tuitions` VALUES ('7', '10,000.00');
INSERT INTO `tuitions` VALUES ('8', '10,000.00');
INSERT INTO `tuitions` VALUES ('9', '10,000.00');
INSERT INTO `tuitions` VALUES ('10', '10,000.00');
INSERT INTO `tuitions` VALUES ('11', '10,000.00');
INSERT INTO `tuitions` VALUES ('12', '10,000.00');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_bin NOT NULL,
  `password` varchar(32) COLLATE utf8_bin NOT NULL,
  `first_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `middle_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(1000) COLLATE utf8_bin NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `role` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT 'teacher',
  `registered_date` date NOT NULL,
  `email_code` varchar(32) COLLATE utf8_bin NOT NULL,
  `subjects` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'administrator', '5f4dcc3b5aa765d61d8327deb882cf99', 'Admin', 'Admin', 'Admin', 'roulegrave_525@yahoo.com', '1', 'admin', '2014-09-19', '3e6a3403c687b6eedad75461d589d403', '');
INSERT INTO `users` VALUES ('7', 'patrick', '6c84cbd30cf9350a990bad2bcc1bec5f', 'Patrick John', 'Pacardo', 'L', '', '1', 'teacher', '2015-01-26', '0a5414fe31b0616ecc5f8b94462ea8f2', '');
INSERT INTO `users` VALUES ('8', 'marilyn', '44d5e42c838f9e300d40c6051da3be6e', 'Marilyn', 'Moncal', 'P', '', '1', 'teacher', '2015-01-28', '1299809711a0bd2caaddd98d3c2aa164', '');
INSERT INTO `users` VALUES ('6', 'emmanuel', '0d0de813c1105498e3435dd2fbf7fa26', 'Emmanuel', 'Litan', 'C', '', '1', 'teacher', '2015-01-26', '8e4d6886ce767ede7c56836d51fa471e', '');
INSERT INTO `users` VALUES ('2', 'accounting', 'd4c143f004d88b7286e6f999dea9d0d7', 'Accounting', 'Accounting', 'Accounting', '', '1', 'accounting', '2015-03-01', 'b156c6b69401c8e87a3c31748323f579', '');
