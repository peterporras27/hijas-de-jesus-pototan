<?php 
session_start();

require_once('core/connect.php'); // connect to database
require_once('core/functions.php'); // General Functions
require_once('core/user-functions.php'); // User functions

// destroy session to Logout user
session_destroy(); 
header('Location: index.php');
exit();
