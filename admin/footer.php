  </div><!-- /#wrapper -->
  <!-- jQuery Version 1.11.0 -->
  <script src="<?php siteurl(); ?>/js/jquery-1.11.0.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?php siteurl(); ?>/js/bootstrap.min.js"></script>
  <!-- Metis Menu Plugin JavaScript -->
  <script src="<?php siteurl(); ?>/js/plugins/metisMenu/metisMenu.min.js"></script>
  <!-- DataTables JavaScript -->
  <script src="<?php siteurl(); ?>/js/plugins/dataTables/jquery.dataTables.js"></script>
  <script src="<?php siteurl(); ?>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
  <script src="<?php siteurl(); ?>/js/bootstrapValidator.min.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?php siteurl(); ?>/js/sb-admin-2.js"></script>
  <script>
    jQuery(document).ready(function() {
      function number_format (number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
          prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
          sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
          dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
          s = '',
          toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
          };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
          s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
          s[1] = s[1] || '';
          s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
      }
      jQuery('#dataTables-teachers, #dataTables-students').dataTable();
      jQuery('input[name="start_year"], input[name="end_year"], input[name="year"]').keyup(function() {
        var numeronly = jQuery(this).val().replace(/[^0-9]/g, '');
        jQuery(this).val(numeronly);
      });

      jQuery('.panel-body').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
      });

      jQuery('input[name="amount_one"], input[name="amount_two"], input[name="fee"], .pricea').keyup(function() {
        var numeronly = jQuery(this).val().replace(/[^0-9,.]/g, '');
        jQuery(this).val(numeronly);
      });

      <?php if (isset($_GET['add']) && $_GET['add']=='student' || isset($_GET['edit'])) { ?>

        var sections = [<?php level_section_script(); ?>];

        jQuery('select[name="level"]').change(function() {
          var chosen = jQuery(this).val();
          jQuery('select[name="section"]').html('<option value="">- Select</option>');
          jQuery('input[name="teacher"]').attr('value', '');
          jQuery.each(sections, function(index, val) {
            if (val.grade == chosen) {
              jQuery('select[name="section"]').append('<option value="'+val.id+'">'+val.name+'</option>');
            }
          });
        });

        jQuery('select[name="section"]').change(function() {
          var advisor = jQuery(this).val();
          jQuery.each(sections, function(index, val) {
            if (val.advisor == advisor) {
              jQuery('input[name="teacher"]').attr('value', val.advisor);
            }
          });
        });

      <?php } ?>

    });
  </script>
</body>
</html>