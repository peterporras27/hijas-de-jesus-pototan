<?php include 'header.php'; ?>
	<!-- Page Content -->
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
        <?php
        $stdntname = '';
        $stndid = (isset($_GET['student']))? preg_replace('/\D/', '', $_GET['student']): '';
        if (!empty($stndid)) {
          $stdntinfo = student_data($stndid, 'first_name', 'last_name');
          $stdntname = ' for '.$stdntinfo['first_name'].' '.$stdntinfo['last_name'];
        }
        if (isset($_GET['add']) && $_GET['add']=='payment') {
          echo 'New Payment Transaction'.$stdntname;
        } else {
          echo 'Payment Transactions'.$stdntname;
        } ?></h1>
      </div><!-- /.col-lg-12 -->
      <div class="col-lg-12">
        <?php if (!isset($_GET['add']) && isset($_GET['student'])) { ?>
        <a href="<?php geturl(); ?>?add=payment&student=<?php echo $_GET['student']; ?>" class="btn btn-info">Add Payment Transaction</a>
        <br><br>
        <?php } ?>
      </div>
    </div><!-- /.row -->
    <div class="row">
    	<div class="col-lg-12">
        <?php if (isset($_GET['success'])) { ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Payment Transaction successfully added!
        </div>
        <?php } ?>
    		<?php
    		if (isset($_GET['add']) && $_GET['add']=='payment') {
          include 'inc/payments-add.php';
        } else {
          include 'inc/payments-list.php';
        } ?>
    	</div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
  </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>