<?php 
// start browser session
session_start();

require_once('connect.php'); // include database connection
require_once('user-functions.php'); // include user functions
require_once('functions.php'); // include general functions
require_once('sms.php'); // include general functions

// check if user is logged in
if (is_online() === true) { 
	$session_uid = $_SESSION['uid']; // get user session ID
	// retrieve user information
	$user = user_data($session_uid, 'uid', 'username', 'first_name', 'last_name', 'email', 'role');
} else {
	// redirect user to login page if the user is not logged in
	header('Location: ../login.php');
	exit();
}