<?php

function sendgrades($to, $msg){

  $apikey = "45e434f3cc4caed1b88b"; //Your API KEY
  $sms = new sendsmsdotpk($apikey); //Making a new sendsms dot pk object

  if ($sms->isValid()) {
    if ($sms->sendsms($to, $msg, 0)){
      echo "Grades Sent successfully to <b>$to</b>!";
    } else {
      echo "error ouccured!";
    }
  } else {
    echo "KEY: " . $apikey . " IS NOT VALID";
  }
}


function sendsms($to, $msg){

  $array = str_split($to,1);
  $num = '';
  if (count($array)==11){
    $x=0;
    foreach ($array as $key) {
      if ($x==0){
        $num .= ($key==0)? '63': $key;
      } else {
        $num .= $key;
      }
      $x++;
    }
  }
  
  echo $num.'<br>';
  $arr_post_body = array(
    "message_type"  => "SEND",
    "mobile_number" => $num,
    "shortcode"     => "2929015680",
    "message_id"    => preg_replace('/\D/', '', md5(uniqid())),
    "message"       => $msg,
    "client_id"     => "3739b79f2333ad3474fbfc478a59ad0b753c888c8a2481a708c05026e38e1916",
    "secret_key"    => "71c630e8f6ebc24e22a322671fa4b897a31ebef5da398e13188ee9fc289965fa"
  );

  $query_string = "";
  $y=0;
  foreach($arr_post_body as $key => $frow) {
    $query_string .= ($y==0)? $key.'='.$frow:'&'.$key.'='.$frow;
    $y++;
  }

  $q = urlencode($query_string);

  $URL = "https://post.chikka.com/smsapi/request";
  $curl_handler = curl_init();
  curl_setopt($curl_handler, CURLOPT_URL, $URL);
  curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
  curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
  curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
  $response = curl_exec($curl_handler);
  curl_close($curl_handler);

  echo "Message Sent Successfully!";
}

class sendsmsdotpk {
   
  private $apikey;
  private $apiUrl = "http://api.smspinoy.com/";

  function sendsmsdotpk($ak){
    $this->apikey = $ak;
  }


  function isValid(){
    $response = $this->fetch_url( $this->api_url("isValid") );
    $obj=json_decode($response);
    if ($obj->isvalid == "Valid")
      return 1;
    return 0;
  }
   

  function getInbox(){
    $response = $this->fetch_url( $this->api_url("inbox") );
    return json_decode($response);
  }
   

  function getOutbox(){
    $response = $this->fetch_url( $this->api_url("outbox") );
    return json_decode($response);
  }

  function sendsms($phone, $msg, $type = 0){
    if (strlen($phone)!=11 || substr($phone, 0, 2) != "09"){
      echo "Phone number you entered is not valid. It should be of 11 characters like 09092196391";
      return false;
    }
    $msg = wordwrap($msg, 300);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->api_url("sendsms")); 
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, "phone=$phone&msg=$msg&type=$type");
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  private function api_url($u){
    return $this->apiUrl . $u . "/" . $this->apikey . ".json";
  }

  private function fetch_url($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($ch, CURLOPT_POST, 1); 
    //curl_setopt($ch, CURLOPT_POSTFIELDS, "a=384gt8gh&p=$phone&m=$msg");
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }


}


?>
