<?php
/*
// Activation Email
//
*/
function activate($email, $email_code) {
	$email 			= sanitize($email);
	$email_code = sanitize($email_code);
	if (mysql_result(mysql_query("SELECT COUNT(`uid`) FROM `users` WHERE `email` = '$email' AND `email_code` = '$email_code' AND `active` = 0"), 0) == 1) {
		mysql_query("UPDATE `users` SET `active` = 1 WHERE `email` = '$email'");
		return true;
	} else {
		return false;
	}
}

/*
// Register a user
*/
function register_user($register_data) {
	array_walk($register_data, 'array_sanitize');
	$register_data['password'] = md5($register_data['password']);
	$fields = '`' . implode('`, `', array_keys($register_data)) . '`';
	$data = '\'' . implode('\', \'', $register_data) . '\'';
	mysql_query("INSERT INTO `users` ($fields) VALUES ($data)");

	if (!empty($register_data['email'])) {
		send_email_code($register_data['email'], 'Account Activation', "\n\nHello ".$register_data['first_name'].",\n\nThank you for registering with us.\nYou may now activate your account using the link below:\n\n ".site_url()."/activate.php?email=".urlencode($register_data['email'])."&email_code=".$register_data['email_code']." \n\n- administrator");
	}
}
/*
// Register a Event
*/
function register_event($register_data) {
	array_walk($register_data, 'array_sanitize');
	$fields = '`' . implode('`, `', array_keys($register_data)) . '`';
	$data = '\'' . implode('\', \'', $register_data) . '\'';
	mysql_query("INSERT INTO `pages` ($fields) VALUES ($data)");
}

function update_event($data) {
	array_walk($data, 'array_sanitize');
	$id = $data['id'];
	$title = $data['title'];
	$content = $data['content'];
	$month = $data['month'];
	$day = $data['day'];
	$year = $data['year'];
	mysql_query("UPDATE `pages` SET `title` = '$title', `content` = '$content', `month` = '$month', `day` = '$day', `year` = '$year' WHERE `id` = $id");
}

/*
// Register a Student
*/
function register_student($register_data) {
	array_walk($register_data, 'array_sanitize');
	$fields = '`' . implode('`, `', array_keys($register_data)) . '`';
	$data = '\'' . implode('\', \'', $register_data) . '\'';
	mysql_query("INSERT INTO `students` ($fields) VALUES ($data)");
}



function request_account_activation($email) {
	$email_code = md5($email + microtime());
	mysql_query("UPDATE `users` SET `email_code` = '$email_code' WHERE `email` = '$email'");
	send_email_code($email, 'Account Activation', "\n\nHi,\n\nThank you for registering with us.\nYou may now activate your account using the link below:\n\n ".site_url()."/activate.php?email=".urlencode($email)."&email_code=".$email_code." \n\n- administrator");
}

/*
// checks if user is logged in. returns TRUE or FALSE
*/
function is_online() {
	return (isset($_SESSION['uid'])) ? true : false;
}

/*
// checks if the user is registered
*/
function user_exists($username) {
	$username = sanitize($username);
	return (mysql_result(mysql_query("SELECT count(`uid`) FROM `users` WHERE `username` = '$username'"), 0) == 1)? true: false;
}

/*
// checks if the user is email is registered
*/
function email_exists($email) {
	$email = sanitize($email);
	return (mysql_result(mysql_query("SELECT count(`uid`) FROM `users` WHERE `email` = '$email'"), 0) == 1)? true: false;
}

/*
// checks if student ID is registered
*/
function studentid_exists($studentid) {
	$studentid = sanitize($studentid);
	return (mysql_result(mysql_query("SELECT count(`uid`) FROM `students` WHERE `studentid` = '$studentid'"), 0) == 1)? true: false;
}

/*
// Boolean
// checks if user email is confirmed
*/
function user_active($username) {
	$username = sanitize($username);
	return (mysql_result(mysql_query("SELECT count(`uid`) FROM `users` WHERE `username` = '$username' AND `active` = 1"), 0) == 1)? 1: false;
}

/*
// retrieves user ID using username
*/
function user_id_from_username($username) {
	$username = sanitize($username);
	return mysql_result(mysql_query("SELECT `uid` FROM `users` WHERE `username` = '$username'"), 0, 'uid');
}


/*
// Login checking function. returns either true or false
*/
function login($username, $password) {
	$user_id = user_id_from_username($username);
	$username = sanitize($username);
	$password = md5($password);
	return (mysql_result(mysql_query("SELECT COUNT(`uid`) FROM `users` WHERE `username` = '$username' AND `password` = '$password'"), 0) == 1)? $user_id : false;
}


/*
// retrieves user data
// Argument: user ID
*/
function user_data($uid) {
	$data = array();
	$uid = (int) $uid;

	$func_num_args = func_num_args();
	$func_get_args = func_get_args();

	if ($func_num_args > 1){
		unset($func_get_args[0]);
		$fields = '`'.implode('`, `', $func_get_args).'`';
		$data = mysql_fetch_assoc(mysql_query("SELECT $fields FROM `users` WHERE `uid` = $uid"));
		return $data;
	}
}

/*
// retrieves Student data
// Argument: student ID
*/
function student_data($uid) {
	$data = array();
	$uid = (int) $uid;

	$func_num_args = func_num_args();
	$func_get_args = func_get_args();

	if ($func_num_args > 1){
		unset($func_get_args[0]);
		$fields = '`'.implode('`, `', $func_get_args).'`';
		$data = mysql_fetch_assoc(mysql_query("SELECT $fields FROM `students` WHERE `uid` = $uid"));
		return $data;
	}
}

/*
// retrieves Sections data
// Argument: section id
*/
function section_data($uid) {
	$data = array();
	$uid = (int) $uid;

	$func_num_args = func_num_args();
	$func_get_args = func_get_args();

	if ($func_num_args > 1){
		unset($func_get_args[0]);
		$fields = '`'.implode('`, `', $func_get_args).'`';
		$data = mysql_fetch_assoc(mysql_query("SELECT $fields FROM `sections` WHERE `sectionid` = $uid"));
		return $data;
	}
}

/*
// retrieves Teachers data
// Argument: section id
*/
function teachers_data($uid) {
	$data = array();
	$uid = (int) $uid;

	$func_num_args = func_num_args();
	$func_get_args = func_get_args();

	if ($func_num_args > 1){
		unset($func_get_args[0]);
		$fields = '`'.implode('`, `', $func_get_args).'`';
		$data = mysql_fetch_assoc(mysql_query("SELECT $fields FROM `users` WHERE `uid` = $uid"));
		return $data;
	}
}

function event_data($uid) {
	$data = array();
	$uid = (int) $uid;

	$func_num_args = func_num_args();
	$func_get_args = func_get_args();

	if ($func_num_args > 1){
		unset($func_get_args[0]);
		$fields = '`'.implode('`, `', $func_get_args).'`';
		$data = mysql_fetch_assoc(mysql_query("SELECT $fields FROM `pages` WHERE `id` = $uid"));
		return $data;
	}
}

/*
// retrieves teacher data
// Argument: teacher id
*/
function teacher_data($uid) {

	$uid = (int) $uid;
	$arr = array();
	if (!empty($uid)){
		$data = mysql_query("SELECT `name`, `level` FROM `sections` WHERE `teacher` = $uid");
		while($teacher = mysql_fetch_array( $data )) {
			$arr[] = $teacher;
		}
		return $arr;
	}
}

/*
// retrieves teacher data
// Argument: teacher id
*/
function subject_data($uid) {

	$data = array();
	$uid = (int) $uid;

	$func_num_args = func_num_args();
	$func_get_args = func_get_args();

	if ($func_num_args > 1){
		unset($func_get_args[0]);
		$fields = '`'.implode('`, `', $func_get_args).'`';
		$data = mysql_fetch_assoc(mysql_query("SELECT $fields FROM `subjects` WHERE `id` = $uid"));
		return $data;
	}
}

/*
// count the number of users registered
// Argument: $status
// 1 for confirmed emails.
// 0 for not fully registered users.
*/
function user_count($status) {
	$status = preg_replace('/\D/', '', $status);
	$status = (empty($status))? 0: $status;
	return mysql_result(mysql_query("SELECT COUNT(`uid`) FROM `users` WHERE `active` = $status"), 0);
}


/*
// Update a Student
*/
function update_student($studentinfo) {
	array_walk($studentinfo, 'array_sanitize');
	$id = $studentinfo['id'];
	$studentid = $studentinfo['studentid'];
	$fname = $studentinfo['first_name'];
	$lname = $studentinfo['last_name'];
	$mname = $studentinfo['middle_name'];
	$section = $studentinfo['section'];
	$phone = $studentinfo['phone'];
	$level = $studentinfo['level'];
	$character = $studentinfo['character'];
	$pass = $studentinfo['password'];

	mysql_query("UPDATE `students` SET `studentid` = '$studentid', `first_name` = '$fname', `last_name` = '$lname', `middle_name` = '$mname', `section` = '$section', `phone` = '$phone', `level` = '$level', `character` = '$character', `password` = '$pass' WHERE `students`.`uid` = $id");
}

function update_teacher($tinfo){

	array_walk($tinfo, 'array_sanitize');

	$uid 				 = $tinfo['uid'];
	$username 	 = $tinfo['username'];
	$password 	 = md5($tinfo['password']);
	$first_name  = $tinfo['first_name'];
	$last_name 	 = $tinfo['last_name'];
	$middle_name = $tinfo['middle_name'];
	$email       = $tinfo['email'];

	mysql_query("UPDATE `users` SET `username` = '$username', `first_name` = '$first_name', `last_name` = '$last_name', `middle_name` = '$middle_name', `password` = '$password', `email` = '$email' WHERE `users`.`uid` = $uid");
}


function update_section($tinfo){

	array_walk($tinfo, 'array_sanitize');

	$uid 	 	 = $tinfo['sectionid'];
	$name 	 = $tinfo['name'];
	$level   = $tinfo['level'];
	$teacher = $tinfo['teacher'];

	mysql_query("UPDATE `sections` SET `name` = '$name', `level` = '$level', `teacher` = '$teacher' WHERE `sectionid` = $uid");
}

function update_subject($tinfo){

	array_walk($tinfo, 'array_sanitize');

	$uid = $tinfo['id'];
	$subject = $tinfo['subject'];
	$student = $tinfo['student'];
	$teacher = $tinfo['teacher'];
	$schoolyear = $tinfo['schoolyear'];
	$first = $tinfo['1st'];
	$second = $tinfo['2nd'];
	$third = $tinfo['3rd'];
	$fourth = $tinfo['4th'];

	mysql_query("UPDATE `subjects` SET `subject` = '$subject', `student` = '$student', `teacher` = '$teacher', `schoolyear` = '$schoolyear', `1st` = '$first', `2nd` = '$second', `3rd` = '$third', `4th` = '$fourth' WHERE `id` = $uid");
}

/*// retrieve all Students*/
function allSections() {
	$sections = mysql_query("SELECT `sectionid`,`name`,`level`,`teacher` FROM `sections` ORDER BY  `sections`.`name` DESC");
	$studentcount = count_students();
	if ($studentcount>=1) {
		$r=0;
		while($section = mysql_fetch_array( $sections )) {
			$color = ($r %2 == 1)? 'odd': 'even';
			$teacher = user_data($section['teacher'], 'first_name', 'middle_name', 'last_name');
				echo '<tr class="'.$color.'">';
        echo '<td>'.$section['name'].'</td>';
        echo '<td>'.$teacher['first_name'].' '.$teacher['middle_name'].' '.$teacher['last_name'].'</td>';
        echo '<td>Grade '.$section['level'].'</td>';
        echo '<td><a class="btn btn-info btn-xs" href="'.get_url().'?edit='.$section['sectionid'].'">Edit</a></td>';
        echo '</tr>';
			$r++;
		}
	}
}

/*// retrieve all Students*/
function studentPaymentHistory($student) {
	$payments = mysql_query("SELECT `student`,`schoolyear`,`amount`,`date`, `method`, `receipt` FROM `payments` WHERE `student` = '$student' ORDER BY  `payments`.`id` DESC");
	$paymentcount = count_payments();
	if ($paymentcount>=1) {
		$r=0;
		while($payment = mysql_fetch_array( $payments )) {
			$color = ($r %2 == 1)? 'odd': 'even';
				$stdntinfo = student_data($payment['student'], 'first_name', 'last_name');
        $stdntname = $stdntinfo['first_name'].' '.$stdntinfo['last_name'];
				echo '<tr class="'.$color.'">';
        echo '<td>'.$stdntname.'</td>';
        echo '<td>'.$payment['schoolyear'].'</td>';
        echo '<td>&#8369; '.$payment['amount'].'</td>';
        echo '<td>'.$payment['date'].'</td>';
        echo '<td>'.$payment['method'].'</td>';
        echo '<td>'.$payment['receipt'].'</td>';
        echo '</tr>';
			$r++;
		}
	}
}

/*// retrieve all Students*/
function paymentTransactions($student, $schoolyear, $fee=0.00) {
	$student = stdntnumber($student);
	$pays = mysql_query("SELECT `student`,`schoolyear`,`amount`,`date`, `method`, `receipt` FROM `payments` WHERE `student` = '$student' ORDER BY `payments`.`id` DESC");
	$pcount = count_payments();
	$totalfees = array();
	if ($pcount>=1) {
		$r=0;
		while($pay = mysql_fetch_array( $pays )) {
			if ($schoolyear==$pay['schoolyear']) {
				$color = ($r %2 == 1)? 'odd': 'even';
				$stdntinfo = student_data($pay['student'], 'first_name', 'last_name');
	      $stdntname = $stdntinfo['first_name'].' '.$stdntinfo['last_name'];
				echo '<tr class="'.$color.'">';
				echo '<td>'.$pay['receipt'].'</td>';
				echo '<td>'.$pay['date'].'</td>';
				echo '<td>'.$pay['method'].'</td>';
	      echo '<td><b>&#8369; '.number_format(preg_replace('/[^0-9.]/', '', $pay['amount']), 2, '.', ',').'</b></td>';
	      echo '</tr>';
	      $totalfees[] = number_format(preg_replace('/[^0-9.]/', '', $pay['amount']), 2, '.', '');
				$r++;
			}
		}

		$ftotal = number_format(array_sum($totalfees), 2, '.', ',');
		$tfee = preg_replace('/[^0-9.]/', '', $ftotal);
		$ffee = preg_replace('/[^0-9.]/', '', $fee);
		$remaining = number_format((float)$ffee - $tfee, 2, '.', ',');
		echo '<tr class="success">';
		echo '<td><b>Total</b></td>';
		echo '<td></td>';
		echo '<td></td>';
		echo '<td><b>&#8369; '.$ftotal;
		echo '</b></td>';
		echo '</tr>';

		echo '</tbody>';
		echo '</table>';

		echo '<table class="table table-striped table-hover">';
		echo '<thead>';
		echo '<tr>';
		echo '<th>Total Fee\'s</th>';
		echo '<th>Total Paid Amount</th>';
		echo '<th>&nbsp;</th>';
		echo '<th>Remaining Balance</th>';
		echo '</tr>';
		echo '</thead>';
		echo '<tbody>';
		echo '<tr class="info">';
		echo '<td><b>&#8369; '.$fee.'</b></td>';
		echo '<td><b>&#8369; '.$ftotal.'</b></td>';
		echo '<td><b>=</b></td>';
		echo '<td><b>&#8369; '.$remaining.'</b></td>';
		echo '</tr>';
	}
}

/*// retrieve all payment transactions*/
function allTransactions() {
	$payments = mysql_query("SELECT `student`,`schoolyear`,`amount`,`date`, `method`, `receipt` FROM `payments` ORDER BY  `payments`.`id` DESC");
	$paymentcount = count_payments();
	if ($paymentcount>=1) {
		$r=0;
		while($payment = mysql_fetch_array( $payments )) {
			$color = ($r %2 == 1)? 'odd': 'even';
				$stdntinfo = student_data($payment['student'], 'first_name', 'last_name');
        $stdntname = $stdntinfo['first_name'].' '.$stdntinfo['last_name'];
				echo '<tr class="'.$color.'">';
        echo '<td>'.$stdntname.'</td>';
        echo '<td>'.$payment['schoolyear'].'</td>';
        echo '<td>&#8369; '.$payment['amount'].'</td>';
        echo '<td>'.$payment['date'].'</td>';
        echo '<td>'.$payment['method'].'</td>';
        echo '<td>'.$payment['receipt'].'</td>';
        echo '</tr>';
			$r++;
		}
	}
}

/*// retrieve all Students*/
function allStudents($advisor='', $role='') {

	$students = mysql_query("SELECT `uid`, `studentid`,`first_name`,`last_name`,`middle_name`,`section`, `level`, `phone` FROM `students` ORDER BY  `students`.`last_name` DESC");
	$studentcount = count_students();
	if ($studentcount>=1) {
		if ($role=='teacher') {
			$r=0;
			while($student = mysql_fetch_array( $students )) {
				$stid = $student['uid'];
				$stdntinfo = student_data($stid, 'uid', 'studentid', 'first_name', 'last_name', 'middle_name', 'section', 'phone', 'level');
				$sname = section_data($stdntinfo['section'], 'name', 'teacher');
				$teacher = user_data($sname['teacher'], 'uid', 'first_name', 'middle_name', 'last_name');
				$color = ($r %2 == 1)? 'odd': 'even';
				if ($advisor==$sname['teacher']){
					echo '<tr class="'.$color.'">';
		      echo '<td>'.$student['first_name'].' '.$student['middle_name'].' '.$student['last_name'].'</td>';
		      echo '<td>'.$sname['name'].'</td>';
		      echo '<td>Grade '.$student['level'].'</td>';
		      echo '<td>'.$teacher['first_name'].' '.$teacher['middle_name'].' '.$teacher['last_name'].'</td>';
		      echo '<td>'.$student['studentid'].'</td>';
		      echo '<td>'.$student['phone'].'</td>';
		      echo '<td><a class="btn btn-success btn-xs" href="'.site_url().'/admin/subjects.php?grades='.$student['uid'].'">View Grades</a></td>';
		      echo '<td><a class="btn btn-info btn-xs" href="'.get_url().'?edit='.$student['uid'].'">Edit</a></td>';
		      echo '</tr>';
					$r++;
				}
			}
		} else {
			$r=0; $x=0;
			while($student = mysql_fetch_array( $students )) {
				$stid = $student['uid'];
				$stdntinfo = student_data($stid, 'uid', 'studentid', 'first_name', 'last_name', 'middle_name', 'section', 'phone', 'level');
				$sname = section_data($stdntinfo['section'], 'name', 'teacher');
				$teacher = user_data($sname['teacher'], 'uid', 'first_name', 'middle_name', 'last_name');
				$color = ($r %2 == 1)? 'odd': 'even';
				$stat2 = ($role=='accounting')? '<a class="btn btn-info btn-xs" href="'.site_url().'/admin/payments.php?student='.$student['uid'].'">Payments</a>': '<a class="btn btn-info btn-xs" href="'.get_url().'?edit='.$student['uid'].'">Info</a>';
				echo '<tr class="'.$color.'">';
	      echo '<td>'.$student['first_name'].' '.$student['middle_name'].' '.$student['last_name'].'</td>';
	      echo '<td>'.$sname['name'].'</td>';
	      echo '<td>Grade '.$student['level'].'</td>';
	      echo '<td>'.$teacher['first_name'].' '.$teacher['middle_name'].' '.$teacher['last_name'].'</td>';
	      echo '<td>'.$student['studentid'].'</td>';
	      echo '<td>'.$student['phone'].'</td>';
	      echo '<td>'.$stat2.'</td>';
	      echo '</tr>';
				$r++;
			}
		}
	}
}

/*// retrieve all teachers*/
function allteachers() {
	$teachers = mysql_query("SELECT `uid`, `username`,`first_name`,`last_name`,`middle_name`,`email`, `active` FROM `users` WHERE `role` = 'teacher' ORDER BY  `users`.`last_name` DESC");
	$studentcount = count_students();
	if ($studentcount>=1) {
		$r=0;
		while($teacher = mysql_fetch_array( $teachers )) {
			$color = ($r %2 == 1)? 'odd': 'even';
			$tinfo = teacher_data($teacher['uid']);
			$count = count($tinfo);
			if ($count>1) {
				$sections = array();
				$levels = array();
				$c=1;
				foreach ($tinfo as $inf) {
					$sections[] = ($c==$count)? '&amp; '.$inf['name']: $inf['name'];
					$levels[] = ($c==$count)? '&amp; '.$inf['level']: $inf['level'];
					$c++;
				}
			} else {
				$sections = (empty($tinfo[0]['name']))? '':$tinfo[0]['name'];
				$levels = (empty($tinfo[0]['level']))? '':$tinfo[0]['level'];
			}

			$section = ($count>1)? implode(", ", $sections): $sections;
			$level = ($count>1)? implode(", ", $levels): $levels;
			$level = (empty($level))? $level: 'Grade '.$level;

			echo '<tr class="'.$color.'">';
      echo '<td>'.$teacher['first_name'].' '.$teacher['middle_name'].' '.$teacher['last_name'].'</td>';
      echo '<td>'.$section.'</td>';
      echo '<td>'.$level.'</td>';
      echo '<td><a class="btn btn-info btn-xs" href="'.get_url().'?edit='.$teacher['uid'].'">Edit</a></td>';
      echo '</tr>';
			$r++;
		}


	}
}

/*// retrieve all events*/
function allevents() {
	$count = mysql_result(mysql_query("SELECT COUNT(`id`) FROM `pages` WHERE `page` = 'event'"), 0);
	if ($count>=1) {
		$events = mysql_query("SELECT `id`, `title`,`page`,`content`,`month`,`year`, `day` FROM `pages` WHERE `page` = 'event' ORDER BY  `pages`.`month` ASC");
		$r=0;
		while($event = mysql_fetch_array( $events )) {
			$color = ($r %2 == 1)? 'odd': 'even';
			$excerpt = (preg_match('/^.{1,50}\b/s', $event['content'], $match))? $match[0]: $event['content'];
			$monthName = date('F', mktime(0, 0, 0, $event['month'], 10));
			echo '<tr class="'.$color.'">';
      echo '<td>'.$event['title'].'</td>';
      echo '<td>'.strip_tags($excerpt).'...</td>';
      echo '<td>'.$monthName.'</td>';
      echo '<td>'.$event['day'].'</td>';
      echo '<td>'.$event['year'].'</td>';
      echo '<td><a class="btn btn-info btn-xs" href="'.get_url().'?edit='.$event['id'].'">Edit</a></td>';
      echo '</tr>';
			$r++;
		}
	}
}

/*// retrieve all events*/
function showallevents() {
	$count = mysql_result(mysql_query("SELECT COUNT(`id`) FROM `pages` WHERE `page` = 'event'"), 0);
	if ($count>=1) {
		$per_page = 10;
		$pages_query = mysql_query("SELECT COUNT(`id`) FROM `pages` WHERE `page` = 'event'");
		$pages = ceil(mysql_result($pages_query, 0) / $per_page);
		$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
		$start = ($page - 1) * $per_page;
		if (isset($_GET['pid'])) {
			$get_events = mysql_query("SELECT * FROM `pages` WHERE `page` = 'event' ORDER BY  `id` DESC") or die(mysql_error());
		}else{
			$get_events = mysql_query("SELECT * FROM `pages` WHERE `page` = 'event' ORDER BY  `id` DESC LIMIT $start, $per_page") or die(mysql_error());
		}
		while($event = mysql_fetch_array( $get_events )) {
			$excerpt = (preg_match('/^.{1,200}\b/s', $event['content'], $match))? $match[0]: $event['content'];
			$date = $event['month'].'/'.$event['day'].'/'.$event['year'];
			if (isset($_GET['pid'])) {
				if ($_GET['pid']==$event['id']) { ?>
				<div>
				<h2><?php echo $event['title']; ?></h2>
				<?php echo $event['content']; ?>
				</div>
				<?php } ?>
			<?php } else { ?>
				<div>
					<h2><?php echo $event['title']; ?></h2>
					<blockquote>
					  <p><?php echo strip_tags($excerpt); ?>...</p>
					  <p><a class="btn btn-info btn-sm" href="events.php?pid=<?php echo $event['id']; ?>">Read More</a></p>
					  <small><cite title="Source Title"><?php echo $date; ?></cite></small>
					</blockquote>
				</div>
				<?php
			}
		}
		if (!isset($_GET['pid'])) {
			echo '<div style=""><b>Pages</b><br><ul class="pagination">';
			if ($pages >= 1 && $page <= $pages) {
				for ($x=1; $x<=$pages; $x++) {
					echo ($x == $page) ? '<li><a href="events.php?page='.$x.'"><b>'.$x.'</b></a></li>' : '<li><a href="events.php?page='.$x.'">'.$x.'</a></li>';
				}
			}
			echo '</ul></div>';
		}
	}
}


/*// retrieve all subjects*/
function allsubjects($year='',$grades='') {

	if (!empty($year) && empty($grades)) {
		$subjects = mysql_query("SELECT `id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th` FROM `subjects` WHERE `schoolyear` = '$year' ORDER BY  `subjects`.`id` DESC");
	} elseif (empty($year) && !empty($grades)) {
		$subjects = mysql_query("SELECT `id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th` FROM `subjects` WHERE `student` = '$grades' ORDER BY  `subjects`.`id` DESC");
	} elseif (!empty($year) && !empty($grades)) {
		$subjects = mysql_query("SELECT `id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th` FROM `subjects` WHERE `student` = '$grades' AND `schoolyear` = '$year' ORDER BY  `subjects`.`id` DESC");
	} else {
		$sy = schoolyear();
		if (!empty($sy)){
			$subjects = mysql_query("SELECT `id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th` FROM `subjects` WHERE `schoolyear` = '$sy' ORDER BY  `subjects`.`id` DESC");
		} else {
			$subjects = mysql_query("SELECT `id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th` FROM `subjects` ORDER BY  `subjects`.`id` DESC");
		}

	}

	$count = count_subjects();
	if ($count>=1) {
		$r=0;
		while($subject = mysql_fetch_array( $subjects )) {
			$student = student_data($subject['student'], 'first_name', 'last_name');
			$getgrade = (isset($_GET['grades']))? '&grades='.$_GET['grades']: '&grades='.$subject['student'];
			$color = ($r %2 == 1)? 'odd': 'even';
			echo '<tr class="'.$color.'">';
      echo '<td>'.$subject['subject'].'</td>';
      echo '<td>'.$student['first_name'].' '.$student['last_name'].'</td>';
      echo '<td>'.$subject['1st'].'</td>';
      echo '<td>'.$subject['2nd'].'</td>';
      echo '<td>'.$subject['3rd'].'</td>';
      echo '<td>'.$subject['4th'].'</td>';
      echo '<td>'.$subject['schoolyear'].'</td>';
      echo '<td><a class="btn btn-info btn-xs" href="'.get_url().'?edit='.$subject['id'].$getgrade.'">Edit Grades</a></td>';
      echo '</tr>';
			$r++;
		}


	}
}

function showgrades($studentid, $sy) {
	$sy = sanitize($sy);
	$sid = stdntid_stdnum($studentid);
	$subjects = mysql_query("SELECT `id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th` FROM `subjects` WHERE `student` = '$sid' AND `schoolyear` = '$sy' ORDER BY  `subjects`.`id` DESC");
	$count = count_stndtsubjects($sid);
	if ($count>=1) {
		$arr = array();
		while($subject = mysql_fetch_array( $subjects )) {
			$s1 = '';
			if ($subject['1st']!='' && $subject['2nd']!='' && $subject['3rd']!='' && $subject['4th']!=''){
				$s1 = solve('+', $subject['1st'], $subject['2nd']);
				$s1 = solve('+', $s1, $subject['3rd']);
				$s1 = solve('+', $s1, $subject['4th']);
				$s1 = solve('/', $s1, 4);
				$arr[] = $s1;
			}

			echo '<tr>';
      echo '<td>'.$subject['subject'].'</td>';
      echo '<td>'.$subject['1st'].'</td>';
      echo '<td>'.$subject['2nd'].'</td>';
      echo '<td>'.$subject['3rd'].'</td>';
      echo '<td>'.$subject['4th'].'</td>';
      echo '<td>'.$s1.'%</td>';
      echo '</tr>';

		}

		$carr = count($arr);
		$total =  ($carr>=1)? solve('/', array_sum($arr), $carr):'';

		echo '<tr class="success">';
    echo '<td><b>Total</b></td>';
    echo '<td></td>';
    echo '<td></td>';
    echo '<td></td>';
    echo '<td></td>';
    echo '<td><b>'.$total.'%</b></td>';
    echo '</tr>';
	}

}

function smsformat($sid, $sy) {

	$sy = (empty($sy))? schoolyear(): $sy;

	$subjects = mysql_query("SELECT `id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th` FROM `subjects` WHERE `student` = '$sid' AND `schoolyear` = '$sy' ORDER BY  `subjects`.`id` DESC");

	$count = count_stndtsubjects($sid);
	if ($count>=1) {
		$arr = array();
		while($subject = mysql_fetch_array( $subjects )) {
			if ($subject['1st']!='' && $subject['2nd']!='' && $subject['3rd']!='' && $subject['4th']!=''){
				$s1 = solve('+', $subject['1st'], $subject['2nd']);
				$s1 = solve('+', $s1, $subject['3rd']);
				$s1 = solve('+', $s1, $subject['4th']);
				$s1 = solve('/', $s1, 4);
				$arr[] = $subject['subject']." ".$s1."%";
			}
		}
		$carr = count($arr);
		$total =  ($carr>=1)? solve('/', array_sum($arr), $carr):'';
		$arr[] = "Total Grade: ".$total."%";
		return implode(", ", $arr);
	}
}

/*
// count the number of students registered
// Argument: $status
*/
function count_students() {
	return mysql_result(mysql_query("SELECT COUNT(`uid`) FROM `students`"), 0);
}

function count_payments() {
	return mysql_result(mysql_query("SELECT COUNT(`id`) FROM `payments`"), 0);
}

function count_subjects() {
	return mysql_result(mysql_query("SELECT COUNT(`id`) FROM `subjects`"), 0);
}

function count_stndtsubjects($stdnt) {
	return mysql_result(mysql_query("SELECT COUNT(`id`) FROM `subjects` WHERE `student` = '$stdnt'"), 0);
}

function stdntid_stdnum($stdnum) {
	$stdnum = sanitize($stdnum);
	return mysql_result(mysql_query("SELECT `uid` FROM `students` WHERE `studentid` = '$stdnum'"), 0, 'uid');
}

function stdntphone($stdid) {
	$stdid = sanitize($stdid);
	return mysql_result(mysql_query("SELECT `phone` FROM `students` WHERE `uid` = '$stdid'"), 0, 'phone');
}


function stdntnumber($stdid) {
	$stdid = sanitize($stdid);
	return mysql_result(mysql_query("SELECT `uid` FROM `students` WHERE `studentid` = '$stdid'"), 0, 'uid');
}

/*
// Delete Student
*/
function delete_student($uid){
	$rid = sanitize($uid);
	mysql_query("DELETE FROM `students` WHERE `students`.`uid` = $rid");
}

/*
// Delete teacher
*/
function delete_teacher($uid){
	$rid = sanitize($uid);
	mysql_query("DELETE FROM `users` WHERE `users`.`uid` = $rid");
}

function delete_event($uid){
	$rid = sanitize($uid);
	mysql_query("DELETE FROM `pages` WHERE `pages`.`id` = $rid");
}

/*
// Delete teacher
*/
function delete_subject($uid){
	$rid = sanitize($uid);
	mysql_query("DELETE FROM `subjects` WHERE `subjects`.`id` = $rid");
}

/*
// Delete Sections
*/
function delete_section($uid){
	$rid = sanitize($uid);
	mysql_query("DELETE FROM `sections` WHERE `sections`.`sectionid` = $rid");
}


