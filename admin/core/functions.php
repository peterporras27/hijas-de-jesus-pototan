<?php

// sends email to the user who requested a reactivation code
function send_email_code($to, $subject, $message){
	mail($to, $subject, $message, 'From: peter.porras@august99.com');
}

// clean arrays to convert HTML tags
function array_sanitize(&$item){
	$item = mysql_real_escape_string($item);
	return $item;
}

// clean the variable to convert HTML tags
function sanitize($data){
	return mysql_real_escape_string($data);
}

function solve($eq='', $yr='', $num='') {
	switch ($eq) {
		case '-':
			return number_format((float)$yr - $num, 2, '.', '');
			break;
		case '*':
			return number_format((float)$yr * $num, 2, '.', '');
			break;
		case '/':
			return number_format((float)$yr / $num, 2, '.', '');
			break;
		case '%':
			return number_format((float)$yr % $num, 2, '.', '');
			break;
		default:
			return number_format((float)$yr + $num, 2, '.', '');
			break;
	}
}

// echo's website url
function siteurl(){
	echo "http://$_SERVER[HTTP_HOST]";
}

// returns website url
function site_url(){
	return "http://$_SERVER[HTTP_HOST]";
}
// echo's Current page url
function geturl(){
	echo "http://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]";
}

// returns current page url
function get_url(){
	return "http://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]";
}

// adding a class in the navigation menu if selected page is active.
function activeNav($pageurl){
	echo ($pageurl==$_SERVER['PHP_SELF'])? ' class="active"':'';
}

// displays all error in html form
function output_errors($errors){
	return '<ul style="list-style:none;padding:0;"><li>'.implode('</li><li>', $errors).'</li></ul>';
}

/*
// checks if the school year exist
*/
function year_exists($schoolyear) {
	$year = sanitize($schoolyear);
	return (mysql_result(mysql_query("SELECT count(`id`) FROM `schoolyear` WHERE `year` = '$year'"), 0) == 1)? true: false;
}

/*
// Register school year
*/
function register_schoolyear($register_year) {
	$syear = sanitize($register_year);
	mysql_query("INSERT INTO `schoolyear` (`year`) VALUES ('$syear')");
}

/*
// Register fees
*/
function register_fee($title, $grade, $fee) {
	$title = sanitize($title);
	$grade = sanitize($grade);
	$fee = sanitize($fee);
	mysql_query("INSERT INTO `fees` (`title`, `grade`, `fee`) VALUES ('$title', '$grade', '$fee')");
}

/*
// Register Tuition Fees
*/
function tuition_fee($grade, $fee) {
	$grade = sanitize($grade);
	$fee = sanitize($fee);
	if (!empty($fee)){
		$count = mysql_result(mysql_query("SELECT COUNT(`grade`) FROM `tuitions` WHERE `grade` = '$grade'"), 0);
		if ($count==0){
			mysql_query("INSERT INTO `tuitions` (`grade`, `tuitionfee`) VALUES ('$grade', '$fee')");
		} else {
			mysql_query("UPDATE `tuitions` SET `tuitionfee` = '$fee' WHERE `grade` = '$grade'");
		}
	}
}

function savepage($pageid, $pagename, $pagecontent) {
	$pageid = sanitize($pageid);
	$pagename = sanitize($pagename);
	$pagecontent = sanitize($pagecontent);
	if (!empty($pageid)) {
		$count = mysql_result(mysql_query("SELECT COUNT(`id`) FROM `pages` WHERE `id` = '$pageid'"), 0);
		if ($count==0){
			mysql_query("INSERT INTO `pages` (`page`, `content`) VALUES ('$pagename', '$pagecontent')");
		} else {
			mysql_query("UPDATE `pages` SET `content` = '$pagecontent' WHERE `id` = '$pageid'");
		}
	}
}

function getPageContent($pageid='') {
	$return='';
	if (!empty($pageid)){
		$ccount = mysql_result(mysql_query("SELECT count(`id`) FROM `pages` WHERE `id` = '$pageid'"), 0);
		if ($ccount==1) {
			$pages = mysql_query("SELECT `content` FROM `pages` WHERE `id` = '$pageid'");
			while( $page = mysql_fetch_array($pages) ) {
				$return = $page['content'];
			}
		}
	}
	return $return;
}


/*
// retrieve all school year
*/
function allSchoolyear() {
	$return = array();
	$years = mysql_query("SELECT `id`, `year` FROM `schoolyear` ORDER BY  `schoolyear`.`year` DESC");
	while($year = mysql_fetch_array( $years )) {
		$return[$year['id']] = $year['year'];
	}
	return $return;
}

/*
// retrieve all school year
*/
function allFees() {
	$return = array();
	$fees = mysql_query("SELECT `id`, `title`, `grade`, `fee` FROM `fees` ORDER BY  `fees`.`grade` DESC");
	$ccount = mysql_result(mysql_query("SELECT count(`id`) FROM `fees`"), 0);
	if ($ccount>=1) {
		while( $fee = mysql_fetch_array($fees) ) {
			$return[$fee['id']] = array($fee['id'], $fee['title'], $fee['grade'], $fee['fee']);
		}
	}
	return $return;
}

/*
// retrieve all school year
*/
function allTuitionFees() {
	$return = array();
	$fees = mysql_query("SELECT `grade`, `tuitionfee` FROM `tuitions` ORDER BY  `tuitions`.`grade` DESC");
	$ccount = mysql_result(mysql_query("SELECT count(`grade`) FROM `tuitions`"), 0);
	if ($ccount>=1) {
		while( $fee = mysql_fetch_array($fees) ) {
			$return[$fee['grade']] = $fee['tuitionfee'];
		}
	}
	return $return;
}

function schoolyear(){
	$years = mysql_query("SELECT `id`, `year` FROM `schoolyear` ORDER BY  `schoolyear`.`year` DESC");
	$x=0;
	while($year = mysql_fetch_array( $years )) {
		if ($x==0){
			$return = (!empty($year['year']))? $year['year']:'';
		}
		$x++;
	}
	return $return;
}

function getadv($teacher){
	$data = mysql_fetch_assoc(mysql_query("SELECT `sectionid` FROM `sections` WHERE `teacher` = '$teacher'"));
	return $data['sectionid'];
}

/*
// Delete school Year
*/
function delete_year($yearid){
	$yid = sanitize($yearid);
	mysql_query("DELETE FROM `schoolyear` WHERE `schoolyear`.`id` = $yid");
}

/*
// Delete school Year
*/
function delete_fee($fee){
	$fee = sanitize($fee);
	mysql_query("DELETE FROM `fees` WHERE `fees`.`id` = $fee");
}

/*
// checks if the section Name exist
*/
function section_exists($sectionname) {
	$section = sanitize($sectionname);
	return (mysql_result(mysql_query("SELECT count(`id`) FROM `sections` WHERE `name` = '$section'"), 0) == 1)? true: false;
}

function countSections() {
	return mysql_result(mysql_query("SELECT count(`sectionid`) FROM `sections`"), 0);
}

function countStudents() {
	return mysql_result(mysql_query("SELECT count(`uid`) FROM `students`"), 0);
}

function countTeachers() {
	return mysql_result(mysql_query("SELECT count(`uid`) FROM `users` WHERE `role` = 'teacher'"), 0);
}

function countSubjects() {
	return mysql_result(mysql_query("SELECT count(`id`) FROM `subjects`"), 0);
}

/*
// Register a Section
*/
function register_section($register_section) {
	array_walk($register_section, 'array_sanitize');
	$fields = '`' . implode('`, `', array_keys($register_section)) . '`';
	$data = '\'' . implode('\', \'', $register_section) . '\'';
	mysql_query("INSERT INTO `sections` ($fields) VALUES ($data)");
}

/*
// Register a Payment Transaction
*/
function register_payment($register_payment) {
	array_walk($register_payment, 'array_sanitize');
	$fields = '`' . implode('`, `', array_keys($register_payment)) . '`';
	$data = '\'' . implode('\', \'', $register_payment) . '\'';
	mysql_query("INSERT INTO `payments` ($fields) VALUES ($data)");
}

/*
// Register a Subject
*/
function register_subject($register_subject) {
	array_walk($register_subject, 'array_sanitize');
	$fields = '`' . implode('`, `', array_keys($register_subject)) . '`';
	$data = '\'' . implode('\', \'', $register_subject) . '\'';
	$return = mysql_query("INSERT INTO `subjects` ($fields) VALUES ($data)");
	return $return;
}

function teachers_dropdown($selected = '') {
	$return = array();
	$teachers = mysql_query("SELECT `uid`, `first_name`, `last_name` FROM `users` WHERE `role` = 'teacher' ORDER BY  `users`.`last_name` ASC");
	while($user = mysql_fetch_array( $teachers )) {
		$chosen = ($selected==$user['uid'])? ' selected': '';
		$return[] = '<option value="'.$user['uid'].'"'.$chosen.'>'.$user['last_name'].', '.$user['first_name'].'</option>';
	}
	echo implode("\n", $return);
}

function students_dropdown($selected = '', $section = '') {
	$return = array();
	$students = mysql_query("SELECT `uid`, `first_name`, `last_name` FROM `students` WHERE `section` = '$section' ORDER BY  `students`.`last_name` ASC");
	if ($students){
		while($user = mysql_fetch_array( $students )) {
			$chosen = ($selected==$user['uid'])? ' selected': '';
			$return[] = '<option value="'.$user['uid'].'"'.$chosen.'>'.$user['last_name'].', '.$user['first_name'].'</option>';
		}
	}
	echo implode("\n", $return);
}

function level_section_script() {
	$return = array();
	$sections = mysql_query("SELECT `sectionid`, `name`, `level`, `teacher` FROM `sections` ORDER BY  `sections`.`name` ASC");
	while($sec = mysql_fetch_array( $sections )) {
		$return[] = '{id:"'.$sec['sectionid'].'", name:"'.$sec['name'].'", grade:"'.$sec['level'].'", advisor:"'.$sec['teacher'].'"}';
	}
	echo implode(",", $return);
}

function section_dropdown($level = '', $selected = '') {
	if (!empty($level)) {
		$return = array();
		$sections = mysql_query("SELECT `sectionid`, `name`, `level`, `teacher` FROM `sections` WHERE `level` = '$level'");
		while($sec = mysql_fetch_array( $sections )) {
			$chosen = ($selected==$sec['sectionid'])? ' selected': '';
			$return[] = '<option value="'.$sec['sectionid'].'"'.$chosen.'>'.$sec['name'].'</option>';
		}
		echo implode("\n", $return);
	}
}