<?php
if (isset($_POST['delete']) && !empty($_POST['delete'])) {
  $suid = preg_replace('/\D/', '', $_POST['delete']);
  delete_student($suid);
  header("Location: ".get_url()."?deleted");
  exit();
}

if (isset($_GET['deleted'])) { ?>
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    Student Deleted successfully!
  </div>
<?php } ?>

<div class="panel panel-default">
  <div class="panel-heading">Student List</div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover" id="dataTables-students">
        <thead>
          <tr>
            <th>Student Name</th>
            <th>Section</th>
            <th>Year Level</th>
            <th>Teacher Advisor</th>
            <th>Student ID</th>
            <th>Mobile Number</th>
            <?php
            if ($user['role']=='teacher') {
              echo '<th>Grades</th>';
            } ?>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          <?php allStudents($user['uid'],$user['role']); ?>
        </tbody>
      </table>
    </div><!-- /.table-responsive -->
  </div><!-- /.panel-body -->
</div><!-- /.panel -->