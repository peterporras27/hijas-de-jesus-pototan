<?php

$subject_err = '';
$student_err = '';
$teacher_err = '';
$schoolyear_err = '';

$subjectid = (isset($_GET['edit']))? preg_replace('/\D/', '', $_GET['edit']): '';

if (!empty($subjectid)) {
	$subjectinfo = subject_data(
		$subjectid,
		'id',
		'subject',
		'student',
		'teacher',
		'schoolyear',
		'1st',
		'2nd',
		'3rd',
		'4th'
	);
}

if (empty($_POST) === false) {

  $$required_fields = array('subject', 'student', 'teacher', 'schoolyear');
  $subject_err    = (empty($_POST['subject']))? ' has-error': '';
  $student_err    = (empty($_POST['student']))? ' has-error': '';
  $teacher_err    = (empty($_POST['teacher']))? ' has-error': '';
  $schoolyear_err = (empty($_POST['schoolyear']))? ' has-error': '';

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true){
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

  if (empty($errors) === true) {
	   $register_data = array(
	    'id'    		 => $subjectid,
	    'subject'    => ucwords($_POST['subject']),
	    'student'    => preg_replace('/\D/', '', $_POST['student']),
	    'teacher'    => preg_replace('/\D/', '', $_POST['teacher']),
	    'schoolyear' => $_POST['schoolyear'],
	    '1st' 			 => $_POST['1st'],
	    '2nd' 			 => $_POST['2nd'],
	    '3rd' 			 => $_POST['3rd'],
	    '4th' 			 => $_POST['4th']
	  );

	  update_subject($register_data);
	  $gr = (!empty($_POST['grades']))? '&grades='.$_POST['grades']:'';
	 	header("Location: ".get_url()."?updated=subject&edit=".$_POST['id'].$gr);
	  exit();
	}

}

$getgrade = (isset($_GET['grades']))? '?grades='.$_GET['grades']:'';

$theteacher = (empty($subjectinfo['teacher']))? $user['uid']: $subjectinfo['teacher'];
$theschoolyear = (empty($subjectinfo['schoolyear']))? schoolyear(): $subjectinfo['schoolyear'];
?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="row">
			<?php if (empty($errors) === false){ ?>
			<div class="alert alert-danger alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <?php echo output_errors($errors); ?>
			</div>
			<?php } ?>

			<?php if (isset($_GET['updated'])) { ?>
			<div class="alert alert-info alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  Subject successfully Updated! <a href="<?php geturl(); ?><?php echo $getgrade; ?>" class="alert-link">Go Back to list.</a>
			</div>
			<?php } ?>

			<form role="form" id="addteacher" method="post">
				<input type="hidden" name="id" value="<?php echo $subjectid; ?>">
				<input type="hidden" name="schoolyear" value="<?php echo $theschoolyear; ?>">
				<input type="hidden" name="teacher" value="<?php echo $theteacher; ?>">
				<?php if (isset($_GET['grades'])) { ?>
				<input type="hidden" name="grades" value="<?php echo $_GET['grades']; ?>">
				<?php } ?>
				<div class="col-md-12 col-sm-12 col-xs-12">
				  <div class="form-group<?php echo $subject_err; ?>">
				    <label>Subject Name*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" name="subject" class="form-control" value="<?php echo $subjectinfo['subject']; ?>">
				  </div>
				 	<input type="hidden" name="student" value="<?php echo (isset($_GET['grades']))? $_GET['grades']:''; ?>">
				  <div class="form-group">
				    <label>1st Grading Grade:</label>
				    <input type="text" name="1st" class="form-control" value="<?php echo $subjectinfo['1st']; ?>">
				  </div>
				  <div class="form-group">
				    <label>2nd Grading Grade:</label>
				    <input type="text" name="2nd" class="form-control" value="<?php echo $subjectinfo['2nd']; ?>">
				  </div>
				  <div class="form-group">
				    <label>3rd Grading Grade:</label>
				    <input type="text" name="3rd" class="form-control" value="<?php echo $subjectinfo['3rd']; ?>">
				  </div>
				  <div class="form-group">
				    <label>4th Grading Grade:</label>
				    <input type="text" name="4th" class="form-control" value="<?php echo $subjectinfo['4th']; ?>">
				  </div>
				  <div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <input type="submit" class="btn btn-lg btn-success btn-block" value="Save">
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <a href="<?php geturl(); ?><?php echo $getgrade; ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12"><br>
							<a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">Delete Subject</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete <?php echo $subjectinfo['subject']; ?></h4>
      </div>
      <div class="modal-body">
        <h4>Note:</h4>
        Are you sure to remove <b><?php echo $subjectinfo['subject']; ?></b>?<br>
        All information associated with the subject <?php echo $subjectinfo['subject']; ?> will be deleted.
      </div>
      <div class="modal-footer">
      	<form method="post" action="<?php geturl(); ?>">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        	<input type="hidden" name="delete" value="<?php echo $subjectid; ?>">
        	<input type="submit" class="btn btn-danger" value="Delete Subject">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->