<?php 

$subject_err = '';
$student_err = '';
$teacher_err = '';
$schoolyear_err = '';

if (empty($_POST) === false) {
  
  $required_fields = array('subject', 'student', 'teacher', 'schoolyear');
  $subject_err    = (empty($_POST['subject']))? ' has-error': '';
  $student_err    = (empty($_POST['student']))? ' has-error': '';
  $teacher_err    = (empty($_POST['teacher']))? ' has-error': '';
  $schoolyear_err = (empty($_POST['schoolyear']))? ' has-error': '';
  
  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true) {
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

}

if (empty($_POST) === false && empty($errors) === true){
  $register_data = array(
    'subject'    => ucwords($_POST['subject']),
    'student'    => preg_replace('/\D/', '', $_POST['student']),
    'teacher'    => preg_replace('/\D/', '', $_POST['teacher']),
    'schoolyear' => $_POST['schoolyear'],
    '1st' 			 => $_POST['1st'],
    '2nd' 			 => $_POST['2nd'],
    '3rd' 			 => $_POST['3rd'],
    '4th' 			 => $_POST['4th']
  );
  
  register_subject($register_data); 
  $ss = (isset($_GET['grades']) && !empty($_GET['grades']))? "&grades=".$_GET['grades']:'';
  header("Location: subjects.php?add=subject&success".$ss);
  exit();
} 

$ret = (isset($_GET['grades']) && !empty($_GET['grades']))? "?grades=".$_GET['grades']:''; 
?>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="row">
			<?php if (empty($errors) === false){ ?>
			<div class="alert alert-danger alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <?php echo output_errors($errors); ?>
			</div>
			<?php } ?>

			<?php if (isset($_GET['success'])) { ?>
			<div class="alert alert-info alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  Subject successfully Added! <a href="<?php geturl(); ?><?php echo $ret; ?>" class="alert-link">Return to subject lists.</a>
			</div>
			<?php } ?>

			<form role="form" id="addteacher" method="post">
				<input type="hidden" name="schoolyear" value="<?php echo schoolyear(); ?>">
				<input type="hidden" name="teacher" value="<?php echo $user['uid']; ?>">
				<div class="col-md-12 col-sm-12 col-xs-12">
				  <div class="form-group<?php echo $subject_err; ?>">
				    <label>Subject Name*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" name="subject" class="form-control">
				  </div>
				  <div class="form-group<?php echo $student_err; ?>">
				    <label>Student*</label> <span><i><small>(required)</small></i></span>
				    <select name="student" class="form-control">
				    	<option value="">- Select</option>
				    	<?php 
				    	$adv = getadv($user['uid']);
				    	$grade = (isset($_GET['grades']) && !empty($_GET['grades']))? $_GET['grades']:'';
				    	students_dropdown($grade, $adv);
				    	?>
				    </select>
				  </div>
				  <div class="form-group">
				    <label>1st Grading Grade:</label>
				    <input type="text" name="1st" class="form-control">
				  </div>
				  <div class="form-group">
				    <label>2nd Grading Grade:</label>
				    <input type="text" name="2nd" class="form-control">
				  </div>
				  <div class="form-group">
				    <label>3rd Grading Grade:</label>
				    <input type="text" name="3rd" class="form-control">
				  </div>
				  <div class="form-group">
				    <label>4th Grading Grade:</label>
				    <input type="text" name="4th" class="form-control">
				  </div>
				  <div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <input type="submit" class="btn btn-lg btn-info btn-block" value="Register">
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <a href="<?php geturl(); ?><?php echo $ret; ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>