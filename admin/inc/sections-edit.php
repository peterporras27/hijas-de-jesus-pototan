<?php 

$name_err = '';
$grade_err = '';

$sectionid = (isset($_GET['edit']))? preg_replace('/\D/', '', $_GET['edit']): '';

if (!empty($sectionid)) {
	$sectionifo = section_data(
		$sectionid, 
		'sectionid', 
		'name', 
		'level', 
		'teacher', 
		'students'
	);
}

if (empty($_POST) === false) {

  $required_fields = array('name', 'level');

  $name_err  = (empty($_POST['name']))? ' has-error': '';
  $grade_err = (empty($_POST['level']))? ' has-error': '';
  

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true){
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

  if (empty($errors) === true) {
	  $register_data = array(
	    'sectionid'	=> $_POST['sectionid'],
	    'name'      => $_POST['name'],
	    'level'     => $_POST['level'],
	    'teacher'   => $_POST['teacher']
	  );
	  
	  update_section($register_data); 
	 	header("Location: ".get_url()."?updated=section&edit=".$_POST['sectionid']);
	  exit();
	} 

}


?>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="row">
			<?php if (empty($errors) === false){ ?>
			<div class="alert alert-danger alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <?php echo output_errors($errors); ?>
			</div>
			<?php } ?>

			<?php if (isset($_GET['updated'])) { ?>
			<div class="alert alert-info alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  Section successfully Updated! <a href="<?php geturl(); ?>" class="alert-link">Return to section lists.</a>
			</div>
			<?php } ?>

			<form role="form" id="addteacher" method="post">
				<input type="hidden" name="sectionid" value="<?php echo $sectionid; ?>">
				<div class="col-md-12 col-sm-12 col-xs-12">
				  <div class="form-group<?php echo $name_err; ?>">
				    <label>Section Name*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" name="name" class="form-control" value="<?php echo $sectionifo['name'];?>">
				  </div>

	
				  <div class="form-group<?php echo $grade_err; ?>">
				    <label>Level*</label> <span><i><small>(required)</small></i></span>
				    <select name="level" class="form-control">
				    	<option value="">- Select</option>
				    	<?php for ($i=1; $i <= 12; $i++) { 
				    		$chosen = ($i==$sectionifo['level'])? ' selected': '';
				    		echo '<option value="'.$i.'"'.$chosen.'>Grade '.$i.'</option>';
				    	}?>
				    </select>
				  </div>
				  <div class="form-group">
				    <label>Teacher Advisor</label> <span><i><small>(optional)</small></i></span>
				    <select class="form-control" name="teacher">
				    	<option value=""></option>
				    	<?php teachers_dropdown($sectionifo['teacher']); ?>
				    </select>
				  </div>
				  <div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <input type="submit" class="btn btn-lg btn-info btn-block" value="Save">
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
							<br/>
							<a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">Delete Section</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete <?php echo $sectionifo['name']; ?></h4>
      </div>
      <div class="modal-body">
        <h4>Note:</h4>
        Are you sure to remove <b><?php echo $sectionifo['name']; ?></b>?<br>
        All information associated with section <?php echo $sectionifo['name']; ?> will be deleted.
      </div>
      <div class="modal-footer">
      	<form method="post" action="<?php geturl(); ?>">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        	<input type="hidden" name="delete" value="<?php echo $sectionid; ?>">
        	<input type="submit" class="btn btn-danger" value="Delete Section">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->