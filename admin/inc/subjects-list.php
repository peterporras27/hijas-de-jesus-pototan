<?php
if (isset($_POST['delete']) && !empty($_POST['delete'])) {
  $suid = preg_replace('/\D/', '', $_POST['delete']);
  delete_subject($suid);
  header("Location: ".get_url()."?deleted");
  exit();
}

if (isset($_GET['deleted'])) { ?>
<div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  Subject Deleted successfully!
</div>
<?php } ?>

<?php
if (isset($_POST['pnumber']) && isset($_POST['txt'])){
  if (!empty($_POST['pnumber']) && !empty($_POST['txt'])) { ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php sendsms($_POST['pnumber'], $_POST['txt']); ?>
    </div>
    <?php
  }
}

?>

<div class="panel panel-default">
  <div class="panel-heading">Subjects</div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover" id="dataTables-teachers">
        <thead>
          <tr>
            <th>Subject Name</th>
            <th>Student</th>
            <th>1st Grading</th>
            <th>2nd Grading</th>
            <th>3rd Grading</th>
            <th>4th Grading</th>
            <th>School Year</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $grades = (isset($_GET['grades']) && !empty($_GET['grades']))? preg_replace('/\D/', '', $_GET['grades']):'';
          $syear = (isset($_GET['schoolyear']) && !empty($_GET['schoolyear']))? $_GET['schoolyear']: '';
          allsubjects($syear,$grades);
          ?>
        </tbody>
      </table>
      <?php if (isset($_GET['grades']) && !empty($_GET['grades'])){ ?>
      <div>
        <!--
        <form method="post">
          <input type="hidden" name="pnumber" value="<?php echo stdntphone($_GET['grades']); ?>">
          <input type="hidden" name="txt" value="<?php echo smsformat($_GET['grades'], $syear); ?>">
          <input type="submit" class="btn btn-success btn-md" value="Send Grades to Student Via SMS">
        </form>
        -->
      </div>
      <?php } ?>
    </div><!-- /.table-responsive -->
  </div><!-- /.panel-body -->
</div><!-- /.panel -->