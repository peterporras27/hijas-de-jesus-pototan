<?php

$title_err = '';

if (isset($_POST) && empty($_POST) === false) {

  $required_fields      = array('title');
  $title_err         = (empty($_POST['title']))? ' has-error': '';

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true){
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

	if (empty($errors) === true){
	  $register_data = array(
	    'page' 		=> 'event',
	    'title'   => $_POST['title'],
	    'content'	=> $_POST['content'],
	    'month'   => $_POST['month'],
	    'day'     => $_POST['day'],
	    'year'    => $_POST['year']
	  );

	  register_event($register_data);
	  header("Location: events.php?add=event&success");
	  exit();
	}
}

?>

<div class="row">
	<div class="col-lg-10 col-md-10">
		<div class="row">

		<?php if (empty($errors) === false){ ?>
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <?php echo output_errors($errors); ?>
		</div>
		<?php } ?>

		<?php if (isset($_GET['success'])) { ?>
		<div class="alert alert-info alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  Event has been registered successfully! <a href="<?php geturl(); ?>" class="alert-link">Return to lists</a>
		</div>
		<?php } ?>

		<form role="form" id="addteacher" method="post">
		<div class="col-md-6 col-sm-6 col-xs-12">
		  <div class="form-group<?php echo $title_err; ?>">
		    <label>Title*</label> <span><i><small>(required)</small></i></span>
		    <input type="text" name="title" class="form-control">
		  </div>
		  <div class="form-group" style="width:123px;display:inline-block;">
		    <label>Month</label>
		    <select name="month" class="form-control">
		    	<option value="1">January</option>
		    	<option value="2">February</option>
		    	<option value="3">March</option>
		    	<option value="4">April</option>
		    	<option value="5">May</option>
		    	<option value="6">June</option>
		    	<option value="7">July</option>
		    	<option value="8">August</option>
		    	<option value="9">September</option>
		    	<option value="10">October</option>
		    	<option value="11">November</option>
		    	<option value="12">December</option>
		    </select>
		  </div>
		  <div class="form-group" style="width:100px;display:inline-block;">
		    <label>Day</label>
		    <select name="day" class="form-control">
		    	<?php for ($i=1; $i <= 31; $i++) {
		    		echo '<option value="'.$i.'">'.$i.'</option>';
		    	} ?>
		    </select>
		  </div>
		  <div class="form-group" style="width:100px;display:inline-block;">
		    <label>Year</label>
		    <input type="text" name="year" value="<?php echo date('Y'); ?>" class="form-control" maxlength="4">
		  </div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="form-group">
		    <label>Content</label>
		    <textarea name="content" rows="10"></textarea>
		  </div>
		</div>
		<div class="col-lg-12">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
				  <input type="submit" class="btn btn-lg btn-info btn-block" value="Save">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
				  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
				</div>
			</div>
		</div>
		</form>

		</div>
	</div>
</div>
<script type="text/javascript" src="<?php siteurl(); ?>/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
  selector: "textarea",
  theme: "modern",
  plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
  ],
  toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
  toolbar2: "print preview media | forecolor backcolor emoticons",
  image_advtab: true,
  templates: [
      {title: 'Test template 1', content: 'Test 1'},
      {title: 'Test template 2', content: 'Test 2'}
  ]
});
</script>