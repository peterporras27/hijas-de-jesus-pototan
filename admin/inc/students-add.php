<?php

$first_name_err = '';
$last_name_err = '';
$middle_name_err = '';
$student_id_err = '';
$level_err = '';
$section_err = '';

if (isset($_POST) && empty($_POST) === false) {

  $required_fields      = array('first_name', 'last_name', 'middle_name', 'student_id', 'level', 'section');
  $first_name_err       = (empty($_POST['first_name']))? ' has-error': '';
  $last_name_err       	= (empty($_POST['last_name']))? ' has-error': '';
  $middle_name_err      = (empty($_POST['middle_name']))? ' has-error': '';
  $student_id_err      	= (empty($_POST['student_id']))? ' has-error': '';
  $level_err      			= (empty($_POST['level']))? ' has-error': '';
  $section_err      		= (empty($_POST['section']))? ' has-error': '';

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true) {
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

  if (empty($errors) === true) {

    if (studentid_exists($_POST['student_id']) === true) {
      $errors[] = 'Sorry, but the Student ID \''.strip_tags($_POST['student_id']).'\' is already taken.';
      $student_id_err = ' has-error';
    }
  }

	if (empty($errors) === true) {
		$pass = (empty($_POST['pass']))? '': md5($_POST['pass']);
		$teacher = array();
		$section = array();
		$subjects = array();
	  $register_data = array(
	    'studentid'      	=> $_POST['student_id'],
	    'first_name'      => ucwords($_POST['first_name']),
	    'last_name'       => ucfirst($_POST['last_name']),
	    'middle_name'     => ucfirst($_POST['middle_name']),
	    'section'					=> preg_replace('/\D/', '', $_POST['section']),
	    'phone'						=> preg_replace('/\D/', '', $_POST['phone']),
	    'level'						=> preg_replace('/\D/', '', $_POST['level']),
	    'character' 			=> $_POST['character'],
	    'password' 				=> $pass
	  );

	  register_student($register_data);
	  header("Location: students.php?add=student&success");
	  exit();
	}
}
?>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="row">
			<div class="col-lg-12">
				<?php if (empty($errors) === false){ ?>
				<div class="alert alert-danger alert-dismissable">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <?php echo output_errors($errors); ?>
				</div>
				<?php } ?>

				<?php if (isset($_GET['success'])) { ?>
				<div class="alert alert-success alert-dismissable">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  Student successfully registered!
				</div>
				<?php } ?>
			</div>

			<form role="form" id="addstudent" method="post">
				<input type="hidden" name="teacher" value="" />
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <div class="form-group<?php echo $first_name_err; ?>">
						    <label>First Name*</label> <span><i><small>(required)</small></i></span>
						    <input type="text" name="first_name" class="form-control">
						  </div>
						  <div class="form-group<?php echo $last_name_err; ?>">
						    <label>Last Name*</label> <span><i><small>(required)</small></i></span>
						    <input type="text" name="last_name" class="form-control">
						  </div>
						  <div class="form-group<?php echo $middle_name_err; ?>">
						    <label>Middle Name*</label> <span><i><small>(required)</small></i></span>
						    <input type="text" name="middle_name" class="form-control">
						  </div>
						  <div class="form-group<?php echo $student_id_err; ?>">
						    <label>Student ID*</label> <span><i><small>(required)</small></i></span>
						    <input type="text" class="form-control" name="student_id">
						  </div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <div class="form-group">
						    <label>Mobile Phone</label> <span><i><small>(optional)</small></i></span>
						    <input type="text" name="phone" class="form-control" placeholder="09">
						  </div>
						  <div class="form-group<?php echo $level_err; ?>">
						    <label>Year Level*</label> <span><i><small>(required)</small></i></span>
						    <select name="level" class="form-control">
						    	<option value="">- Select</option>
						    	<?php for ($i=0; $i <= 12; $i++) {
						    		echo '<option value="'.$i.'"'.$chosen.'>Grade '.$i.'</option>';
						    	} ?>
						    </select>
						  </div>
						  <div class="form-group">
						    <label>Section</label> <span><i><small>(required)</small></i></span>
						    <select name="section" class="form-control<?php echo $section_err; ?>">
						    	<option value="">- Select</option>
						    </select>
						  </div>
						  <div class="form-group">
						    <label>Student Password</label>
						    <input type="password" class="form-control" name="pass" value="">
						  </div>
						</div>
						<div class="col-lg-12">
							<div class="row">
								<div class="col-md-3 col-sm-3 col-xs-6">
								  <input type="submit" class="btn btn-lg btn-info btn-block" value="Register">
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6">
								  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-12">

						<label>Character Reference:</label>
						<div class="form-group">
							<textarea name="character" width="100%" rows="10" class="form-control"></textarea>
						</div>

				</div>

			</form>
		</div>
	</div>
</div>