<?php 

$name_err = '';
$grade_err = '';

if (empty($_POST) === false) {
  
  $required_fields = array('section_name', 'section_grade');
  $name_err        = (empty($_POST['section_name']))? ' has-error': '';
  $grade_err       = (empty($_POST['section_grade']))? ' has-error': '';
  
  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true) {
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

  if (empty($errors) === true) {
    if (section_exists($_POST['section_name']) === true){
      $errors[] = 'Sorry, but the section name \''.strip_tags($_POST['section_name']).'\' is already taken.';
      $name_err = ' has-error';
    }
  }
}

if (empty($_POST) === false && empty($errors) === true){
  $register_data = array(
    'name'      => ucwords($_POST['section_name']),
    'level'     => $_POST['section_grade'],
    'teacher'   => $_POST['advisor']
  );
  
  register_section($register_data); 
  header("Location: sections.php?add=section&success");
  exit();
} 

?>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="row">
			<?php if (empty($errors) === false){ ?>
			<div class="alert alert-danger alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <?php echo output_errors($errors); ?>
			</div>
			<?php } ?>

			<?php if (isset($_GET['success'])) { ?>
			<div class="alert alert-info alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  Section successfully registered! <a href="<?php geturl(); ?>" class="alert-link">Return to section lists.</a>
			</div>
			<?php } ?>

			<form role="form" id="addteacher" method="post">
				<div class="col-md-12 col-sm-12 col-xs-12">
				  <div class="form-group<?php echo $name_err; ?>">
				    <label>Section Name*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" name="section_name" class="form-control">
				  </div>
				  <div class="form-group<?php echo $grade_err; ?>">
				    <label>Level*</label> <span><i><small>(required)</small></i></span>
				    <select name="section_grade" class="form-control">
				    	<option value="">- Select</option>
				    	<option value="1">Grade 1</option>
				    	<option value="2">Grade 2</option>
				    	<option value="3">Grade 3</option>
				    	<option value="4">Grade 4</option>
				    	<option value="5">Grade 5</option>
				    	<option value="6">Grade 6</option>
				    	<option value="7">Grade 7</option>
				    	<option value="8">Grade 8</option>
				    	<option value="9">Grade 9</option>
				    	<option value="10">Grade 10</option>
				    	<option value="11">Grade 11</option>
				    	<option value="12">Grade 12</option>
				    </select>
				  </div>
				  <div class="form-group">
				    <label>Teacher Advisor</label> <span><i><small>(optional)</small></i></span>
				    <select class="form-control" name="advisor">
				    	<option value=""></option>
				    	<?php teachers_dropdown(); ?>
				    </select>
				  </div>
				  <div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <input type="submit" class="btn btn-lg btn-info btn-block" value="Register">
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
						  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>