<?php

$title_err = '';
$year_err = '';
$eventid = (isset($_GET['edit']))? preg_replace('/\D/', '', $_GET['edit']): '';

if (!empty($eventid)) {
	$eventinfo = event_data(
		$eventid,
		'id',
		'title',
		'content',
		'month',
		'day',
		'year'
	);
	$em = $eventinfo['month'];
}
if (isset($_POST) && empty($_POST) === false) {

  $required_fields      = array('title','year');
  $title_err         = (empty($_POST['title']))? ' has-error': '';
  $year_err         = (empty($_POST['year']))? ' has-error': '';

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true){
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

	if (empty($errors) === true){
	  $register_data = array(
	    'id' 			=> $eventid,
	    'title'   => $_POST['title'],
	    'content'	=> $_POST['content'],
	    'month'   => $_POST['month'],
	    'day'     => $_POST['day'],
	    'year'    => $_POST['year']
	  );

	  update_event($register_data);
	  header("Location: events.php?success=&edit=".$eventid);
	  exit();
	}
}

function selctd($sel, $data){
	echo ($sel==$data)? ' selected':'';
}
?>

<div class="row">
	<div class="col-lg-10 col-md-10">
		<div class="row">

		<?php if (empty($errors) === false){ ?>
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <?php echo output_errors($errors); ?>
		</div>
		<?php } ?>

		<?php if (isset($_GET['success'])) { ?>
		<div class="alert alert-info alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  Event has been Updated successfully! <a href="<?php geturl(); ?>" class="alert-link">Return to lists</a>
		</div>
		<?php } ?>

		<form role="form" id="addteacher" method="post">
		<div class="col-md-6 col-sm-6 col-xs-12">
		  <div class="form-group<?php echo $title_err; ?>">
		    <label>Title*</label> <span><i><small>(required)</small></i></span>
		    <input type="text" name="title" class="form-control" value="<?php echo $eventinfo['title']; ?>">
		  </div>
		  <div class="form-group" style="width:123px;display:inline-block;">
		    <label>Month</label>
		    <select name="month" class="form-control">
		    	<option value="1"<?php selctd(1, $em); ?>>January</option>
		    	<option value="2"<?php selctd(2, $em); ?>>February</option>
		    	<option value="3"<?php selctd(3, $em); ?>>March</option>
		    	<option value="4"<?php selctd(4, $em); ?>>April</option>
		    	<option value="5"<?php selctd(5, $em); ?>>May</option>
		    	<option value="6"<?php selctd(6, $em); ?>>June</option>
		    	<option value="7"<?php selctd(7, $em); ?>>July</option>
		    	<option value="8"<?php selctd(8, $em); ?>>August</option>
		    	<option value="9"<?php selctd(9, $em); ?>>September</option>
		    	<option value="10"<?php selctd(10, $em); ?>>October</option>
		    	<option value="11"<?php selctd(11, $em); ?>>November</option>
		    	<option value="12"<?php selctd(12, $em); ?>>December</option>
		    </select>
		  </div>
		  <div class="form-group" style="width:100px;display:inline-block;">
		    <label>Day</label>
		    <select name="day" class="form-control">
		    	<?php for ($i=1; $i <= 31; $i++) {
		    		$sel = ($i==$eventinfo['day'])? ' selected':'';
		    		echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
		    	} ?>
		    </select>
		  </div>
		  <div class="form-group<?php echo $year_err; ?>" style="width:100px;display:inline-block;">
		    <label>Year</label>
		    <input type="text" name="year" value="<?php echo $eventinfo['year']; ?>" class="form-control" maxlength="4">
		  </div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="form-group">
		    <label>Content</label>
		    <textarea name="content" rows="10"><?php echo $eventinfo['content']; ?></textarea>
		  </div>
		</div>
		<div class="col-lg-12">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
				  <input type="submit" class="btn btn-lg btn-info btn-block" value="Save">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
				  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
					<a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">Delete Event</a>
				</div>
			</div>
		</div>
		</form>

		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete <?php echo $eventinfo['title']; ?></h4>
      </div>
      <div class="modal-body">
        <h4>Are you sure?</h4>
      </div>
      <div class="modal-footer">
      	<form method="post" action="<?php geturl(); ?>">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        	<input type="hidden" name="delete" value="<?php echo $eventid; ?>">
        	<input type="submit" class="btn btn-danger" value="Delete Event">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="<?php siteurl(); ?>/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ]
});
</script>