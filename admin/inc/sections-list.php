<?php
if (isset($_POST['delete']) && !empty($_POST['delete'])) {
  $suid = preg_replace('/\D/', '', $_POST['delete']);
  delete_section($suid);
  header("Location: ".get_url()."?deleted");
  exit();
}

if (isset($_GET['deleted'])) { ?>
<div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  Section Deleted successfully!
</div>
<?php } ?>

<div class="panel panel-default">
  <div class="panel-heading">Sections</div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover" id="dataTables-teachers">
        <thead>
          <tr>
            <th>Section Name</th>
            <th>Advisory Teacher</th>
            <th>Year Level</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          <?php allSections(); ?>
        </tbody>
      </table>
    </div><!-- /.table-responsive -->
  </div><!-- /.panel-body -->
</div><!-- /.panel -->