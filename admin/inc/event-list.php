<?php
if (isset($_POST['delete']) && !empty($_POST['delete'])) {
  $suid = preg_replace('/\D/', '', $_POST['delete']);
  delete_event($suid);
  header("Location: ".get_url()."?deleted");
  exit();
}

if (isset($_GET['deleted'])) { ?>
<div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  Event Deleted successfully!
</div>
<?php } ?>


<div class="panel panel-default">
  <div class="panel-heading">Events</div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover" id="dataTables-teachers">
        <thead>
          <tr>
            <th>Title</th>
            <th>Excerpt</th>
            <th>Month</th>
            <th>Day</th>
            <th>Year</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody>
          <?php allevents(); ?>
        </tbody>
      </table>

    </div><!-- /.table-responsive -->
  </div><!-- /.panel-body -->
</div><!-- /.panel -->