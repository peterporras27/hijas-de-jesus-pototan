<?php
$studentid_err ='';
$firstname_err = '';
$lastname_err = '';
$middlename_err = '';
$address_err = '';
$username_err = '';
$level_err = '';
$phone_err ='';
$email_err = '';
$expiry_err = '';


$stdntid = (isset($_GET['edit']))? preg_replace('/\D/', '', $_GET['edit']): '';

if (!empty($stdntid)) {
	$stdntinfo = student_data(
		$stdntid,
		'uid',
		'studentid',
		'first_name',
		'last_name',
		'middle_name',
		'section',
		'phone',
		'level',
		'character',
		'password'
	);

	$section = section_data($stdntinfo['uid'], 'name', 'teacher');
}

if (empty($_POST) === false) {

  $required_fields = array(
  	'studentid',
  	'first_name',
  	'last_name',
  	'middle_name',
  	'level',
  );

  $studentid_err  = (empty($_POST['studentid']))? ' has-error': '';
  $firstname_err  = (empty($_POST['first_name']))? ' has-error': '';
  $lastname_err   = (empty($_POST['last_name']))? ' has-error': '';
  $middlename_err = (empty($_POST['middle_name']))? ' has-error': '';
  $level_err  		= (empty($_POST['level']))? ' has-error': '';

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true){
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

  if (empty($errors) === true) {
    if (studentid_exists($_POST['studentid']) === true) {
    	if ($_POST['studentid'] != $stdntinfo['studentid']) {
    		$errors[] = 'The Student ID you entered is already Taken.';
      	$studentid_err = ' has-error';
    	}
    }
  }

  if (empty($errors) === true) {
  	$pass = (empty($_POST['pass']))? $stdntinfo['password']: md5($_POST['pass']);
	  $register_data = array(
			'id' 					=> preg_replace('/\D/', '',$_POST['id']),
			'studentid' 	=> $_POST['studentid'],
			'first_name' 	=> ucwords($_POST['first_name']),
			'last_name' 	=> ucwords($_POST['last_name']),
			'middle_name' => ucfirst($_POST['middle_name']),
			'section' 		=> preg_replace('/\D/', '', $_POST['section']),
			'phone' 			=> preg_replace('/\D/', '', $_POST['phone']),
			'level' 			=> preg_replace('/\D/', '', $_POST['level']),
			'character' 	=> $_POST['character'],
			'password' 		=> $pass
	  );

	  update_student($register_data);
	  header("Location: ".get_url()."?updated=student&edit=".$_POST['id']);
	  exit();
	}
}

?>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="row">
		<div class="col-lg-12">
			<?php if (empty($errors) === false){ ?>
			<div class="alert alert-danger alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <?php echo output_errors($errors); ?>
			</div>
			<?php } ?>

			<?php if (isset($_GET['updated']) && empty($errors) === true) { ?>
			<div class="alert alert-success alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  Student Info Updated!
			</div>
			<?php } ?>
		</div>

		<form role="form" id="addstudent" method="post">
		<input type="hidden" name="save" value="saved" />
		<input type="hidden" name="id" value="<?php echo $stdntid; ?>" />
		<div class="col-md-7 col-sm-7 col-xs-12">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <div class="form-group<?php echo $firstname_err; ?>">
				    <label>First Name*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" name="first_name" class="form-control"value="<?php echo $stdntinfo['first_name']; ?>">
				  </div>
				  <div class="form-group<?php echo $lastname_err; ?>">
				    <label>Last Name*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" name="last_name" class="form-control" value="<?php echo $stdntinfo['last_name']; ?>">
				  </div>
				  <div class="form-group<?php echo $middlename_err; ?>">
				    <label>Middle Name*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" name="middle_name" class="form-control" value="<?php echo $stdntinfo['middle_name']; ?>">
				  </div>
				  <div class="form-group<?php echo $studentid_err; ?>">
				    <label>Student ID*</label> <span><i><small>(required)</small></i></span>
				    <input type="text" class="form-control" name="studentid" value="<?php echo $stdntinfo['studentid']; ?>">
				  </div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
				    <label>Mobile Phone</label> <span><i><small>(optional)</small></i></span>
				    <input type="text" name="phone" class="form-control" placeholder="09" value="<?php echo $stdntinfo['phone']; ?>">
				  </div>
				  <div class="form-group<?php echo $level_err; ?>">
				    <label>Year Level*</label> <span><i><small>(required)</small></i></span>
				    <select name="level" class="form-control">
				    	<option value="">- Select</option>
				    	<?php for ($i=0; $i <= 12; $i++) {
				    		$chosen = ($stdntinfo['level']==$i)? ' selected': '';
				    		echo '<option value="'.$i.'"'.$chosen.'>Grade '.$i.'</option>';
				    	} ?>
				    </select>
				  </div>
				  <div class="form-group">
				    <label>Section</label> <span><i><small>(required)</small></i></span>
				    <select name="section" class="form-control">
				    	<option value="">- Select</option>
				    	<?php section_dropdown($stdntinfo['level'], $stdntinfo['section']); ?>
				    </select>
				  </div>
				  <div class="form-group">
				    <label>Update Student Password</label>
				    <input type="password" class="form-control" name="pass" value="">
				  </div>

				</div>
				<div class="col-lg-12">
					<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-6">
						  <input type="submit" class="btn btn-lg btn-info btn-block" value="Save">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6">
						  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
							<a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">Delete Student</a>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="col-md-5 col-sm-5 col-xs-12">

				<label>Character Reference:</label>
				<div class="form-group">
					<textarea name="character" width="100%" rows="10" class="form-control"><?php echo $stdntinfo['character']; ?></textarea>
				</div>

		</div>

		</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete <?php echo $stdntinfo['first_name']; ?></h4>
      </div>
      <div class="modal-body">
        <h4>Are you sure?</h4>
        Removing <b><?php echo $stdntinfo['first_name']; ?></b>'s account will be irreversible. <br>
        All information associated with <?php echo $stdntinfo['first_name']; ?>'s account will be deleted.
      </div>
      <div class="modal-footer">
      	<form method="post" action="<?php geturl(); ?>">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        	<input type="hidden" name="delete" value="<?php echo $stdntid; ?>">
        	<input type="submit" class="btn btn-danger" value="Delete Student">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->