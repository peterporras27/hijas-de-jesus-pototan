<?php

$amount_err = '';
$errors = array();
if (empty($_POST) === false) {

  $required_fields = array('amount_one');
  $amount_err        = (empty($_POST['amount_one']))? ' has-error': '';

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true) {
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

	if (empty($errors) === true){
		$decimal = (empty($_POST['amount_two']))? 00: $_POST['amount_two'];
		$amount = $_POST['amount_one'].'.'.preg_replace('/\D/', '', $_POST['amount_two']);
	  $register_data = array(
	    'student'    => preg_replace('/\D/', '', $_POST['student']),
	    'schoolyear' => $_POST['schoolyear'],
	    'amount'   	 => $amount,
	    'date' 			 => date("F j Y, g:i a"),
	    'method'		 => $_POST['method'],
	    'receipt'		 => $_POST['receipt']
	  );

	  register_payment($register_data);
	  header("Location: payments.php?student=".$_POST['student']."&success");
	  exit();
	}
}
?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="row">
			<?php if (empty($errors) === false){ ?>
			<div class="alert alert-danger alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <?php echo output_errors($errors); ?>
			</div>
			<?php } ?>

			<form role="form" id="addteacher" method="post">
				<input type="hidden" name="student" value="<?php echo $_GET['student']; ?>">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<label>Payment Method</label> <span><i><small>(required)</small></i></span>
						<div class="radio">
							<label>
								<input type="radio" name="method" value="cash" checked> Cash
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="method" value="check"> Check
							</label>
						</div>
					</div>
				  <div class="form-group">
				  	<label>School Year</label>
				  	<select name="schoolyear" class="form-control">
							<?php
							foreach (allSchoolyear() as $key => $value) {
		            echo '<option value="'.$value.'">'.$value.'</option>';
		          } ?>
	          </select>
					</div>
					<div class="form-group">
						<label>Receipt Number</label> <span><i><small>(optional)</small></i></span><br>
						<input type="text" name="receipt" class="form-control" placeholder="" value="">
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Amount*</label> <span><i><small>(required)</small></i></span><br>
						  <div class="form-group input-group<?php echo $amount_err; ?>">
								<span class="input-group-addon">&#8369;</span>
								<input type="text" name="amount_one" class="form-control" placeholder="0000" maxlength="6" value="">
								<span class="input-group-addon"><b>.</b></span>
								<input type="text" name="amount_two" class="form-control" placeholder="00" maxlength="2">
							</div>
						</div>
					</div>
				  <div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6"><br><br>
						  <input type="submit" class="btn btn-lg btn-info btn-block" value="Save">
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6"><br><br>
						  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>