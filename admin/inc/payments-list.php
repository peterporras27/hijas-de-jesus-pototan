<div class="panel panel-default">
  <div class="panel-heading">Payment Transaction History</div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover" id="dataTables-teachers">
        <thead>
          <tr>
            <th>Student Name</th>
            <th>School Year</th>
            <th>Payment Amount</th>
            <th>Payment Date</th>
            <th>Payment Method</th>
            <th>Receipt Number</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (isset($_GET['student'])){
            studentPaymentHistory($_GET['student']);
          } else {
            allTransactions();
          } ?>
        </tbody>
      </table>
    </div><!-- /.table-responsive -->
  </div><!-- /.panel-body -->
</div><!-- /.panel -->