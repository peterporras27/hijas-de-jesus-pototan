<?php 

$username_err = '';
$password_err = '';
$password_repeat_err = '';
$first_name_err = '';
$last_name_err = '';
$middle_name_err = '';

if (isset($_POST) && empty($_POST) === false) {

  $required_fields      = array(
  													'username', 
  													'password', 
  													'password_repeat', 
  													'first_name', 
  													'last_name', 
  													'middle_name'
  												);
  $username_err         = (empty($_POST['username']))? ' has-error': '';
  $password_err         = (empty($_POST['password']))? ' has-error': '';
  $password_repeat_err  = (empty($_POST['password_repeat']))? ' has-error': '';
  $first_name_err       = (empty($_POST['first_name']))? ' has-error': '';
  $last_name_err       	= (empty($_POST['last_name']))? ' has-error': '';
  $middle_name_err      = (empty($_POST['middle_name']))? ' has-error': '';

  foreach ($_POST as $key => $value) {
    if (empty($value) && in_array($key, $required_fields) === true){
      $errors[] = 'Kindly fill all the required fields.';
      break 1;
    }
  }

  if (empty($errors) === true){
    
    if (user_exists($_POST['username']) === true){
      $errors[] = 'Sorry, but the username \''.strip_tags($_POST['username']).'\' is already taken.';
      $username_err = ' has-error';
    }

    if (preg_match("/\\s/", $_POST['username']) == true){
      $errors[] = 'Your username must not contain any spaces.';
      $username_err = ' has-error';
    }

    if (strlen($_POST['username']) > 32){
      $errors[] = 'Your username must be at lower than 32 characters.';
      $username_err = ' has-error';
    }

    if (strlen($_POST['password']) < 6){
      $errors[] = 'Your password must be at least 6 characters.';
      $password_err = ' has-error';
      $password_repeat_err = ' has-error';
    }

    if ($_POST['password'] !== $_POST['password_repeat']){
      $errors[] = 'Your passwords do not match.';
      $password_err = ' has-error';
      $password_repeat_err = ' has-error';
    }
  }

	if (empty($errors) === true){
	  $register_data = array(
	    'username'        => strtolower($_POST['username']),
	    'password'        => $_POST['password'],
	    'first_name'      => ucwords($_POST['first_name']),
	    'last_name'       => ucfirst($_POST['last_name']),
	    'middle_name'     => ucfirst($_POST['middle_name']),
	    'email'           => $_POST['email'],
	    'active'          => 1,
	    'role'						=> 'teacher',
	    'registered_date' => date('Y-m-d'),
	    'email_code'      => md5($_POST['username'] + microtime())
	  );
	  
	  register_user($register_data); 
	  header("Location: teachers.php?add=teacher&success");
	  exit();
	} 
} 

?>

<div class="row">
	<div class="col-lg-7 col-md-7">
		<div class="row">
		
		<?php if (empty($errors) === false){ ?>
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <?php echo output_errors($errors); ?>
		</div>
		<?php } ?>

		<?php if (isset($_GET['success'])) { ?>
		<div class="alert alert-info alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  Teacher has been registered successfully! <a href="<?php geturl(); ?>" class="alert-link">Return to lists</a>
		</div>
		<?php } ?>

		<form role="form" id="addteacher" method="post">
		<div class="col-md-6 col-sm-6 col-xs-12">
		  <div class="form-group<?php echo $first_name_err; ?>">
		    <label>First Name*</label> <span><i><small>(required)</small></i></span>
		    <input type="text" name="first_name" class="form-control">
		  </div>
		  <div class="form-group<?php echo $last_name_err; ?>">
		    <label>Last Name*</label> <span><i><small>(required)</small></i></span>
		    <input type="text" name="last_name" class="form-control">
		  </div>
		  <div class="form-group<?php echo $middle_name_err; ?>">
		    <label>Middle Name*</label> <span><i><small>(required)</small></i></span>
		    <input type="text" name="middle_name" class="form-control">
		  </div>
		  <div class="form-group">
		    <label>Email</label> <span><i><small>(optional)</small></i></span>
		    <input type="email" name="email" class="form-control">
		  </div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
		  <div class="form-group<?php echo $username_err; ?>">
		    <label>Username*</label> <span><i><small>(required)</small></i></span>
		    <input type="text" class="form-control" name="username">
		  </div>
		  <div class="form-group<?php echo $password_err; ?>">
		    <label>Password*</label> <span><i><small>(Must be at least 6 character or higher)</small></i></span>
		    <input type="password" name="password" class="form-control">
		  </div>
		  <div class="form-group<?php echo $password_repeat_err; ?>">
		    <label>Repeat Password*</label> <span><i><small>(required)</small></i></span>
		    <input type="password" name="password_repeat" class="form-control">
		  </div>
		</div>
		<div class="col-lg-12">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
				  <input type="submit" class="btn btn-lg btn-info btn-block" value="Register">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
				  <a href="<?php geturl(); ?>" class="btn btn-lg btn-default btn-block">Cancel</a>
				</div>
			</div>
		</div>
		</form>

		</div>
	</div>
</div>