<?php

include 'header.php';

$sart_err = '';
$end_err = '';
$errors = array();

if ($user['role']=='admin') {

  if (empty($_POST) === false) {

    $required_fields = array('start_year', 'end_year');
    $sart_err        = (empty($_POST['start_year']))? ' has-error': '';
    $end_err         = (empty($_POST['end_year']))? ' has-error': '';

    foreach ($_POST as $key => $value) {
      if (empty($value) && in_array($key, $required_fields) === true) {
        $errors[] = 'Kindly fill all the required fields.';
        break 1;
      }
    }

    if (empty($errors) === true) {

      if (year_exists($_POST['start_year'].'-'.$_POST['end_year']) === true) {
        $errors[] = '<b>ERROR!</b> - Sorry, but the School Year \''.strip_tags($_POST['start_year'].'-'.$_POST['end_year']).'\' already exist.';
      }
      if ($_POST['start_year'] > $_POST['end_year']) {
        $errors[] = '<b>ERROR!</b> - Start Year should always be lower than the End Year.';
      }
    }
  }
  if (empty($_POST) === false && empty($errors) === true){
    register_schoolyear($_POST['start_year'].'-'.$_POST['end_year']);
    header("Location: settings.php?success");
    exit();
  }

  if (isset($_GET['delete']) && !empty($_GET['delete'])) {
    $yearid = preg_replace('/\D/', '', $_GET['delete']);
    delete_year($yearid);
    header("Location: settings.php?success=&deleted=");
    exit();
  }

}

if ($user['role']=='accounting') {
  $fee_err = array();
  if (empty($_POST) === false) {

    if ($_POST['tuitionfee']=='true') {

      $z=1;$y=0;
      foreach ($_POST['tuition'] as $key => $value) {
        if (empty($value)) {
          if ($y==1){$errors[] = 'Kindly fill all the required fields.';}
          $fee_err[] = $z;
          $y++;
        }
        $z++;
      }

      if (empty($errors) === true) {
        foreach ($_POST['tuition'] as $key => $value) {
          tuition_fee($key, $value);
        }
        //header("Location: settings.php?success");
        //exit();
      }




    } else {

      $required_fields = array('title', 'fee');
      $sart_err        = (empty($_POST['title']))? ' has-error': '';
      $end_err         = (empty($_POST['fee']))? ' has-error': '';

      foreach ($_POST as $key => $value) {
        if (empty($value) && in_array($key, $required_fields) === true) {
          $errors[] = 'Kindly fill all the required fields.';
          break 1;
        }
      }

      if (empty($errors) === true) {
        register_fee($_POST['title'], $_POST['grade'], $_POST['fee']);
        header("Location: settings.php?success");
        exit();
      }
    }

  }

  if (isset($_GET['delete']) && !empty($_GET['delete'])) {
    $yearid = preg_replace('/\D/', '', $_GET['delete']);
    delete_fee($yearid);
    header("Location: settings.php?success=&deleted=");
    exit();
  }

}


?>
		<!-- Page Content -->
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">General Settings</h1>
        </div><!-- /.col-lg-12 -->
      </div><!-- /.row -->
      <div class="row">
        <div class="col-lg-12">

          <?php if ($user['role']=='admin') { ?>

          <div class="panel panel-info">
            <div class="panel-heading">
              <b>School Year</b>
            </div><!-- /.panel-heading -->
            <div class="panel-body">
              <div class="col-md-12">
                <?php if (empty($errors) === false){ ?>
                <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo output_errors($errors); ?>
                </div>
                <?php } ?>

                <?php if (isset($_GET['success'])) { ?>
                <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php
                  if (isset($_GET['deleted'])){
                    echo 'School Year Deleted Successfully.';
                  } else {
                    echo 'School Year Added Successfully.';
                  }?>
                </div>
                <?php } ?>
              </div>
              <div class="col-md-6 col-xs-12">
                 <div class="panel panel-default">
                  <div class="panel-heading">
                    <b>School Years</b>
                  </div><!-- /.panel-heading -->
                  <div class="panel-body">
                    <div class="alert alert-info">
                      <p><b>Note:</b> The top most highlighted in green is the current school year.</p>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>School Year</th>
                          <th>Options</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $x=0;
                        foreach (allSchoolyear() as $key => $value) {
                          echo ($x==0)? '<tr class="success">':'<tr class="danger">';
                          echo '<td>'.$value.'</td>';
                          if ($x!=0){
                            echo '<td><a href="'.get_url().'?delete='.$key.'" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="right" title="" data-original-title="Are you sure?">Delete</a></td>';
                          } else {
                            echo '<td>&nbsp;</td>';
                          }
                          echo '</tr>';
                          $x++;
                        }
                        ?>
                      </tbody>
                    </table>
                 </div><!-- /.panel-body -->
                </div><!-- /.panel -->
              </div>
              <div class="col-md-6 col-xs-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <b>Add School Year</b>
                  </div><!-- /.panel-heading -->
                  <div class="panel-body">
                    <form method="post" action="<?php geturl(); ?>">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group<?php echo $sart_err; ?>">
                            <label>Start Year</label>
                            <input class="form-control" maxlength="4" placeholder="<?php echo date('Y')-1; ?>" name="start_year" value="">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group<?php echo $end_err; ?>">
                            <label>End Year</label>
                            <input class="form-control" maxlength="4" placeholder="<?php echo date('Y'); ?>" name="end_year" value="">
                          </div>
                        </div>
                      </div>
                      <input type="submit" class="btn btn-info" value="ADD">
                    </form>
                  </div><!-- /.panel-body -->
                </div><!-- /.panel -->
              </div>
            </div><!-- /.panel-body -->
          </div><!-- /.panel -->

          <?php } ?>

          <?php if ($user['role']=='accounting') { ?>
            <?php if (empty($errors) === false) { ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo output_errors($errors); ?>
            </div>
            <?php } ?>
          <div class="panel panel-info">
            <div class="panel-heading">
              <b>Fees</b>
            </div><!-- /.panel-heading -->
            <div class="panel-body">
              <div class="col-md-12">
                <?php if (isset($_GET['success'])) { ?>
                <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php
                  if (isset($_GET['deleted'])){
                    echo 'Fee Deleted Successfully.';
                  } else {
                    echo 'Fee Added Successfully.';
                  }?>
                </div>
                <?php } ?>
              </div>
              <div class="col-md-8 col-xs-12">
                 <div class="panel panel-default">
                  <div class="panel-heading">
                    <b>List of Fees</b>
                  </div><!-- /.panel-heading -->
                  <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Title</th>
                          <th>Grade</th>
                          <th>Fee</th>
                          <th>Options</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $x=0;
                        foreach (allFees() as $key) {
//                          var_dump($key);
                          $color = ($x %2 == 1)? 'odd': 'even';
                          echo '<tr class="'.$color.'">';
                          echo '<td>'.$key[1].'</td>';
                          echo '<td>Grade '.$key[2].'</td>';
                          echo '<td>&#8369; '.$key[3].'</td>';
                          echo '<td><a href="'.get_url().'?delete='.$key[0].'" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="right" title="" data-original-title="Are you sure?">Delete</a></td>';

                          echo '</tr>';
                          $x++;
                        }
                        ?>
                      </tbody>
                    </table>
                 </div><!-- /.panel-body -->
                </div><!-- /.panel -->
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <b>Create Additional Fee</b>
                  </div><!-- /.panel-heading -->
                  <div class="panel-body">
                    <form method="post" action="<?php geturl(); ?>">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group<?php echo $sart_err; ?>">
                            <label>Title</label>
                            <input class="form-control" placeholder="" name="title" value="">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Grade</label>
                            <select name="grade" class="form-control">
                              <?php
                              for ($i=1; $i <= 12; $i++) {
                                $chosen = ($stdntinfo['level']==$i)? ' selected': '';
                                echo '<option value="'.$i.'"'.$chosen.'>Grade '.$i.'</option>';
                              } ?>
                            </select>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <label>Fee</label>
                          <div class="form-group input-group<?php echo $end_err; ?>">
                            <span class="input-group-addon">&#8369;</span>
                            <input type="text" name="fee" class="form-control" placeholder="00.00" value="">
                          </div>
                        </div>

                      </div>
                      <input type="submit" class="btn btn-info" value="ADD">
                    </form>
                  </div><!-- /.panel-body -->
                </div><!-- /.panel -->
              </div>
            </div><!-- /.panel-body -->
          </div><!-- /.panel -->
          <div class="panel panel-success">
            <div class="panel-heading">
              <b>Tuition Fees</b>
            </div><!-- /.panel-heading -->
            <div class="panel-body">
              <div class="col-md-6 col-sm-6 col-xs-12">
              <form method="post" action="<?php geturl(); ?>">
                <input type="hidden" name="tuitionfee" value="true">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Year Level</th>
                      <th>Annual Tuition Fee</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php for ($i=1; $i <= 12; $i++) {
                      $pulltuitions = allTuitionFees();
                      $tfees = (empty($pulltuitions)===false)? allTuitionFees():'';
                      $v = (is_array($tfees))? $tfees[$i]:'';
                      $ccode = ($i %2 == 1)? 'odd': 'even';
                      $feeerr = (in_array($i, $fee_err))? ' has-error': ''; ?>
                      <tr class="<?php echo $ccode; ?>">
                        <td style="width:50%;">Grade <?php echo $i; ?></td>
                        <td>
                          <div class="form-group input-group<?php echo $feeerr; ?>">
                            <span class="input-group-addon">&#8369;</span>
                            <input type="text" name="tuition[<?php echo $i; ?>]"
                            class="form-control price" placeholder="000.00" value="<?php echo $v; ?>">
                          </div>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <input type="submit" class="btn btn-success btn-lg" value="Save">
              </form>
              </div>
            </div>
          </div>
          <?php } ?>
        </div><!-- /.col-lg-8 -->
      </div><!-- /.row -->
    </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>