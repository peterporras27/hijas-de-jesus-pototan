<?php include 'header.php'; ?>
	<!-- Page Content -->
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
        <?php
        if (isset($_GET['add']) && $_GET['add']=='subject') {
          echo 'Add a subject';
        } elseif (isset($_GET['grades'])) {
          $stdinf = student_data($_GET['grades'], 'first_name', 'last_name');
          echo 'Grades of '.$stdinf['first_name'].' '.$stdinf['last_name'];
        } else {
          echo 'Subjects';
        } ?></h1>
      </div><!-- /.col-lg-12 -->
      <div class="col-lg-12">
        <?php if (!isset($_GET['add'])) { ?>
        <?php if (!isset($_GET['edit'])) {?>
        <?php if ($user['role']=='teacher') { $grade = (isset($_GET['grades']))? '&grades='.$_GET['grades']:'';?>
        <a href="<?php geturl(); ?>?add=subject<?php echo $grade;?>" class="btn btn-info" style="float:left;margin-right:15px;">Add New Subject</a>
        <?php } ?>
        <form method="get" role="form">
          <div class="pull-right">
            <div style="float:left;margin-right:15px;margin-top: 7px;">School Years:</div> <select name="schoolyear" class="form-control" style="width:150px;float:left;margin-right:10px;">
            <?php
            $scyear = (isset($_GET['schoolyear']))? $_GET['schoolyear']: '';
            foreach (allSchoolyear() as $key => $value) {
              $sel = ($scyear==$value)? ' selected':'';
              echo '<option value="'.$value.'"'.$sel.'>'.$value.'</option>';
            }
            ?>
            </select>
            <input type="submit" class="btn btn-success"  value="Filter">
          </div>
          </form>
         <?php } ?>
          <div class="clearfix"></div>
          <br>
        <?php } ?>
      </div>
    </div><!-- /.row -->
    <div class="row">
    	<div class="col-lg-12">
    		<?php
    		if (isset($_GET['add']) && $_GET['add']=='subject') {
          include 'inc/subjects-add.php';
        } elseif (isset($_GET['edit']) && !empty($_GET['edit'])) {
          include 'inc/subjects-edit.php';
        } else {
          include 'inc/subjects-list.php';
        } ?>
    	</div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
  </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>