<?php include 'header.php'; ?>
		<!-- Page Content -->
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
          <?php 
          if (isset($_GET['add']) && $_GET['add']=='student') { 
            echo 'Register Student';
          } else { 
            echo 'Students'; 
          } ?>
          </h1>
        </div><!-- /.col-lg-12 -->
        <div class="col-lg-12">
          <?php if (!isset($_GET['add'])) { ?>
          <a href="<?php geturl(); ?>?add=student" class="btn btn-success">Register New Student</a>
          <br><br>
          <?php } ?>
        </div>
      </div><!-- /.row -->
      <div class="row">
      	<div class="col-lg-12">
      		<?php 
          $checkedit = (isset($_GET['edit']))? preg_replace('/\D/', '', $_GET['edit']):'';

      		if (isset($_GET['add']) && $_GET['add']=='student') { 
          	include 'inc/students-add.php'; 
          } elseif (!empty($checkedit)) {
            include 'inc/students-edit.php'; 
          } else {
          	include 'inc/students-list.php'; 
          }
          ?>
      	</div>
      </div>
    </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>