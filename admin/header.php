<?php include 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>CIC - Colegio De La Inmaculada Concepcion</title>
  <!-- Bootstrap Core CSS -->
  <link href="<?php siteurl(); ?>/css/bootstrap.min.css" rel="stylesheet">
  <!-- MetisMenu CSS -->
  <link href="<?php siteurl(); ?>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
  <!-- DataTables CSS -->
  <link href="<?php siteurl(); ?>/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
  <!-- Custom Fonts -->
  <link href="<?php siteurl(); ?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Form Validation CSS -->
  <link href="<?php siteurl(); ?>/css/bootstrapValidator.min.css" rel="stylesheet" type="text/css">
  <!-- Custom CSS -->
  <link href="<?php siteurl(); ?>/css/sb-admin.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php siteurl(); ?>">Hijas De Jesus</a>
      </div><!-- /.navbar-header -->
      <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
             <li><a href="<?php siteurl(); ?>/admin/teachers.php?edit=<?php echo $user['uid'];?>"><i class="fa fa-gear fa-fw"></i>User Settings</a></li>
            <li class="divider"></li>
            <li><a href="<?php siteurl(); ?>/admin/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
          </ul><!-- /.dropdown-user -->
        </li>
      </ul>
      <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
          <ul class="nav" id="side-menu">

            <li<?php activeNav('/admin/index.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <?php if ($user['role']=='admin') { ?>
            <li<?php activeNav('/admin/events.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/events.php"><i class="fa fa-calendar fa-fw"></i> Events</a>
            </li>
            <?php } ?>
            <?php if ($user['role']=='admin' || $user['role']=='accounting') { ?>
            <li<?php activeNav('/admin/settings.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/settings.php"><i class="fa fa-cog fa-fw"></i> General Settings</a>
            </li>
            <?php } ?>
            <?php if ($user['role']=='admin') { ?>
            <li<?php activeNav('/admin/sections.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/sections.php"><i class="fa fa-sitemap fa-fw"></i> Class Sections</a>
            </li>

            <li<?php activeNav('/admin/teachers.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/teachers.php"><i class="fa fa-user fa-fw"></i> Teachers</a>
            </li>

            <?php } ?>

            <li<?php activeNav('/admin/students.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/students.php"><i class="fa fa-users fa-fw"></i> Students</a>
            </li>

            <?php if ($user['role']=='teacher') { ?>

            <li<?php activeNav('/admin/subjects.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/subjects.php"><i class="fa fa-book fa-fw"></i> Subjects</a>
            </li>

            <?php } ?>

            <?php if ($user['role']=='accounting') { ?>

            <li<?php activeNav('/admin/payments.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/payments.php"><i class="fa fa-dollar fa-fw"></i> Payments</a>
            </li>

            <?php } ?>

            <!-- <li<?php activeNav('/admin/news.php'); ?>>
              <a href="<?php siteurl(); ?>/admin/news.php"><i class="fa fa-comments fa-fw"></i> News Feeds</a>
            </li> -->

          </ul>
        </div><!-- /.sidebar-collapse -->
      </div><!-- /.navbar-static-side -->
    </nav>