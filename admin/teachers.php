<?php include 'header.php'; ?>
	<!-- Page Content -->
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
        <?php 
        if (isset($_GET['add']) && $_GET['add']=='teacher') { 
          echo 'Register Teacher';
        } else { 
          echo 'Advisory Teachers'; 
        } ?>
        </h1>
      </div><!-- /.col-lg-12 -->
      <div class="col-lg-12">
        <?php if (!isset($_GET['add'])) { ?>
        <a href="<?php geturl(); ?>?add=teacher" class="btn btn-info">Register New Teacher</a>
        <br><br>
        <?php } ?>
      </div>
    </div><!-- /.row -->
    <div class="row">
      <div class="col-lg-12">
        <?php 
        if (isset($_GET['add']) && $_GET['add']=='teacher') {
          include 'inc/teachers-add.php';
        }  elseif (isset($_GET['edit']) && !empty($_GET['edit'])) {
          include 'inc/teachers-edit.php';
        } else {
          include 'inc/teachers-list.php';
        } ?>
      </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
  </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>