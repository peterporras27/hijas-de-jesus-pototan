<?php include 'header.php'; ?>
	<!-- Page Content -->
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
        <?php
        if (isset($_GET['add']) && $_GET['add']=='event') {
          echo 'Add Event';
        } else {
          echo 'Events';
        } ?>
        </h1>
      </div><!-- /.col-lg-12 -->
      <div class="col-lg-12">
        <?php if (!isset($_GET['add'])) { ?>
        <a href="<?php geturl(); ?>?add=event" class="btn btn-info">Add Event</a>
        <br><br>
        <?php } ?>
      </div>
    </div><!-- /.row -->
    <div class="row">
      <div class="col-lg-12">
        <?php
        if (isset($_GET['add']) && $_GET['add']=='event') {
          include 'inc/event-add.php';
        }  elseif (isset($_GET['edit']) && !empty($_GET['edit'])) {
          include 'inc/event-edit.php';
        } else {
          include 'inc/event-list.php';
        } ?>
      </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
  </div><!-- /#page-wrapper -->
<?php include 'footer.php'; ?>