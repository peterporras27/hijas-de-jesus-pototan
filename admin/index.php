<?php

include 'header.php';
$alert=array();
if ($user['role']=='admin') {
	if (isset($_POST['homepage'])) {
		if (!empty($_POST['homepage'])) {
			savepage(1, 'homepage', $_POST['homepage']);
			$alert[] = '<div class="row">';
			$alert[] = '<div class="col-lg-12">';
			$alert[] = '<div class="alert alert-info alert-dismissable">';
			$alert[] = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
			$alert[] = 'Homepage successfully Updated!';
			$alert[] = '</div>';
			$alert[] = '</div>';
			$alert[] = '</div>';
		}
	}
	if (isset($_POST['faculty'])) {
		if (!empty($_POST['faculty'])) {
			savepage(2, 'faculty', $_POST['faculty']);
			$alert[] = '<div class="row">';
			$alert[] = '<div class="col-lg-12">';
			$alert[] = '<div class="alert alert-info alert-dismissable">';
			$alert[] = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
			$alert[] = 'Faculty and Staff successfully Updated!';
			$alert[] = '</div>';
			$alert[] = '</div>';
			$alert[] = '</div>';
		}
  }
}
$homepage = getPageContent(1);
$faculty = getPageContent(2);
?>
		<!-- Page Content -->
		<div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Dashboard</h1>
        </div><!-- /.col-lg-12 -->
      </div><!-- /.row -->
      <div class="row">
			  <div class="col-lg-3 col-md-6">
		      <div class="panel panel-primary">
	          <div class="panel-heading">
	            <div class="row">
	              <div class="col-xs-3">
	                <i class="fa fa-book fa-5x"></i>
	              </div>
	              <div class="col-xs-9 text-right">
	                <div class="huge"><?php echo countSubjects(); ?></div>
	                <div>Subjects</div>
	              </div>
	            </div>
	          </div>

	            <div class="panel-footer">
	              <!-- <span class="pull-left">View Details</span> -->
	              <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	              <div class="clearfix"></div>
	            </div>

		      </div>
			  </div>
			  <div class="col-lg-3 col-md-6">
		      <div class="panel panel-green">
	          <div class="panel-heading">
	            <div class="row">
	              <div class="col-xs-3">
	                <i class="fa fa-users fa-5x"></i>
	              </div>
	              <div class="col-xs-9 text-right">
	                <div class="huge"><?php echo countStudents(); ?></div>
	                <div>Students</div>
	              </div>
	            </div>
	          </div>

	            <div class="panel-footer">
	              <!-- <span class="pull-left">View Details</span> -->
	              <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	              <div class="clearfix"></div>
	            </div>

		      </div>
			  </div>
			  <div class="col-lg-3 col-md-6">
		      <div class="panel panel-yellow">
	          <div class="panel-heading">
	            <div class="row">
	              <div class="col-xs-3">
	                <i class="fa fa-user fa-5x"></i>
	              </div>
	              <div class="col-xs-9 text-right">
	                <div class="huge"><?php echo countTeachers(); ?></div>
	                <div>Advisory Teachers</div>
	              </div>
	            </div>
	          </div>

	            <div class="panel-footer">
	              <!-- <span class="pull-left">View Details</span> -->
	              <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	              <div class="clearfix"></div>
	            </div>

		      </div>
			  </div>
			  <div class="col-lg-3 col-md-6">
		      <div class="panel panel-red">
	          <div class="panel-heading">
	            <div class="row">
	              <div class="col-xs-3">
	                <i class="fa fa-sitemap fa-5x"></i>
	              </div>
	              <div class="col-xs-9 text-right">
	                <div class="huge"><?php echo countSections(); ?></div>
	                <div>Sections</div>
	              </div>
	            </div>
	          </div>

            <div class="panel-footer">
              <!-- <span class="pull-left">View Details</span> -->
              <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
              <div class="clearfix"></div>
            </div>

		      </div>
			  </div>
			</div><!-- /.row -->
			<?php
			if ($user['role']=='admin') {
				echo implode("\n", $alert); ?>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<script type="text/javascript" src="<?php siteurl(); ?>/js/tinymce/tinymce.min.js"></script>
					<script type="text/javascript">
					tinymce.init({
					  selector: "textarea",
					  theme: "modern",
					  plugins: [
					      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
					      "searchreplace wordcount visualblocks visualchars code fullscreen",
					      "insertdatetime media nonbreaking save table contextmenu directionality",
					      "emoticons template paste textcolor colorpicker textpattern"
					  ],
					  toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
					  toolbar2: "print preview media | forecolor backcolor emoticons",
					  image_advtab: true,
					  templates: [
					      {title: 'Test template 1', content: 'Test 1'},
					      {title: 'Test template 2', content: 'Test 2'}
					  ]
					});
					</script>
					<form method="post">
						<h2>Home Page Contents</h2>
				    <textarea name="homepage" rows="10"><?php echo $homepage; ?></textarea>
				    <br><input type="submit" class="btn btn-lg btn-success" value="SAVE">
					</form>
					<form method="post">
				    <h2>Faculty and Staff</h2>
				    <textarea name="faculty" rows="10"><?php echo $faculty; ?></textarea>
				    <br><input type="submit" class="btn btn-lg btn-success" value="SAVE">
					</form>
					<br><br>
				</div>
			</div>
			<?php } ?>
    </div><!-- /#page-wrapper -->

<?php include 'footer.php'; ?>