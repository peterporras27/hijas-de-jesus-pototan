
-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `schoolyear`;
CREATE TABLE `schoolyear` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `schoolyear` (`id`, `year`) VALUES
(13,	'2014-2015');

DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections` (
  `sectionid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1999) NOT NULL,
  `level` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `students` longtext NOT NULL,
  PRIMARY KEY (`sectionid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `sections` (`sectionid`, `name`, `level`, `teacher`, `students`) VALUES
(9,	'3rd Year A',	11,	8,	''),
(8,	'4th Year A',	12,	7,	''),
(7,	'4th Year B',	12,	6,	'');

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT,
  `studentid` varchar(32) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `section` varchar(32) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `level` int(11) NOT NULL,
  `subjects` longtext NOT NULL,
  `balance` varchar(100) NOT NULL,
  `paid` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `students` (`uid`, `studentid`, `first_name`, `last_name`, `middle_name`, `section`, `phone`, `level`, `subjects`, `balance`, `paid`) VALUES
(8,	'0125',	'Shiela Mae',	'Cerbas',	'L',	'7',	'',	12,	'',	'',	''),
(12,	'0126',	'Marites',	'Esmante',	'G',	'7',	'',	12,	'',	'',	''),
(11,	'0124',	'Judy Ann',	'Adonis',	'B',	'7',	'',	12,	'',	'',	''),
(6,	'0123',	'Roulegrave',	'Gamboa',	'S',	'7',	'09464575779',	12,	'',	'',	''),
(10,	'0127',	'Ernalyn',	'Espada',	'A',	'7',	'',	12,	'',	'',	''),
(13,	'0111',	'Mark Louie',	'ParreÃ±o',	'Y',	'8',	'651651651',	12,	'',	'1000',	'10000'),
(14,	'0112',	'Shiela Mae',	'Rendaje',	'A',	'8',	'',	12,	'',	'',	''),
(15,	'0113',	'Sharmaine Anne',	'Rendaje',	'A',	'8',	'',	12,	'',	'',	''),
(16,	'0114',	'Rhenly',	'Fango',	'G',	'8',	'',	12,	'',	'',	''),
(17,	'0115',	'Jason',	'Porras',	'L',	'8',	'',	12,	'',	'',	''),
(18,	'3210',	'Jeren',	'Pastor',	'V',	'9',	'09093323605',	11,	'',	'',	'');

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(32) NOT NULL,
  `student` varchar(32) NOT NULL,
  `teacher` varchar(32) NOT NULL,
  `schoolyear` varchar(32) NOT NULL,
  `1st` varchar(32) NOT NULL,
  `2nd` varchar(32) NOT NULL,
  `3rd` varchar(32) NOT NULL,
  `4th` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `subjects` (`id`, `subject`, `student`, `teacher`, `schoolyear`, `1st`, `2nd`, `3rd`, `4th`) VALUES
(18,	'Science',	'13',	'7',	'2014-2015',	'75',	'75',	'75',	'75'),
(12,	'Math',	'11',	'6',	'2014-2015',	'80',	'85',	'88',	'85'),
(13,	'Math',	'8',	'6',	'2014-2015',	'87',	'88',	'85',	'84'),
(14,	'Math',	'12',	'6',	'2014-2015',	'86',	'84',	'85',	'88'),
(15,	'Math',	'10',	'6',	'2014-2015',	'85',	'85',	'84',	'86'),
(16,	'Math',	'6',	'6',	'2014-2015',	'88',	'85',	'86',	'84'),
(17,	'Science',	'17',	'7',	'2014-2015',	'85',	'85',	'84',	'87'),
(19,	'Science',	'16',	'7',	'2014-2015',	'85',	'85',	'85',	'85'),
(20,	'Science',	'14',	'7',	'2014-2015',	'86',	'84',	'85',	'85'),
(21,	'Science',	'15',	'7',	'2014-2015',	'86',	'84',	'84',	'85'),
(22,	'Computer',	'18',	'8',	'2014-2015',	'99',	'85',	'75',	'65');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_bin NOT NULL,
  `password` varchar(32) COLLATE utf8_bin NOT NULL,
  `first_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `middle_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(1000) COLLATE utf8_bin NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `role` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT 'teacher',
  `registered_date` date NOT NULL,
  `email_code` varchar(32) COLLATE utf8_bin NOT NULL,
  `students` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `users` (`uid`, `username`, `password`, `first_name`, `last_name`, `middle_name`, `email`, `active`, `role`, `registered_date`, `email_code`, `students`) VALUES
(1,	'administrator',	'5f4dcc3b5aa765d61d8327deb882cf99',	'Admin',	'Admin',	'Admin',	'roulegrave_525@yahoo.com',	1,	'admin',	'2014-09-19',	'3e6a3403c687b6eedad75461d589d403',	''),
(7,	'patrick',	'6c84cbd30cf9350a990bad2bcc1bec5f',	'Patrick John',	'Pacardo',	'L',	'',	1,	'teacher',	'2015-01-26',	'0a5414fe31b0616ecc5f8b94462ea8f2',	''),
(8,	'marilyn',	'44d5e42c838f9e300d40c6051da3be6e',	'Marilyn',	'Moncal',	'P',	'',	1,	'teacher',	'2015-01-28',	'1299809711a0bd2caaddd98d3c2aa164',	''),
(6,	'emmanuel',	'0d0de813c1105498e3435dd2fbf7fa26',	'Emmanuel',	'Litan',	'C',	'',	1,	'teacher',	'2015-01-26',	'8e4d6886ce767ede7c56836d51fa471e',	'');

-- 2015-02-28 17:36:36
