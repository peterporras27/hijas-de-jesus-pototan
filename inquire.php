<?php include 'header.php'; ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<br><br>
				<?php
				if (isset($_POST['studentid']) && !empty($_POST['studentid'])) {
					if (studentid_exists($_POST['studentid'])===false) { ?>
						<div class="alert alert-dismissable alert-danger">
						  <button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>Error!</strong> The student ID you entered does not exist, please try again.
						</div>
						<?php
					}
				} ?>

			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php
				if (isset($_POST['studentid'])) {
					if (studentid_exists($_POST['studentid'])===true) {
						$tuitionfee = allTuitionFees();
						$totalfees = array();
						$sid = stdntid_stdnum($_POST['studentid']);
						$student = student_data($sid, 'first_name', 'last_name', 'middle_name', 'character', 'level','password');
						$checking = (empty($student['password']))? md5(''): $student['password'];
						if ($checking == md5($_POST['studentpass']) ) { ?>
							<h2><?php echo $student['first_name'].' '.$student['middle_name'].' '.$student['last_name'];?></h2>
							<h3>School Year: <?php echo htmlentities($_POST['schoolyear']); ?></h3>
							<br>
							<div class="panel panel-success">
		            <div class="panel-heading">
		              <b>Grades of <?php echo $student['first_name'].' '.$student['middle_name'].' '.$student['last_name'];?></b>
		            </div><!-- /.panel-heading -->
		            <div class="panel-body">
		            	<table class="table table-striped table-hover ">
									  <thead>
									    <tr>
									      <th>Subjects</th>
									      <th>1st</th>
									      <th>2nd</th>
									      <th>3rd</th>
									      <th>4th</th>
									      <th>Total</th>
									    </tr>
									  </thead>
									  <tbody>
									    <?php showgrades($_POST['studentid'], $_POST['schoolyear']); ?>
									  </tbody>
									</table>
									<div class="panel panel-success">
				            <div class="panel-heading">
				              <b>Character Reference</b>
				            </div><!-- /.panel-heading -->
				            <div class="panel-body">
											<?php echo $student['character']; ?>
										</div>
									</div>
		            </div>
		          </div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="panel panel-success">
				            <div class="panel-heading">
				              <b>Student Fees</b>
				            </div><!-- /.panel-heading -->
				            <div class="panel-body">
				            	<table class="table table-striped table-hover">
											  <thead>
											    <tr>
											      <th>Fee</th>
											      <th>Amount</th>
											    </tr>
											  </thead>
											  <tbody>
											  	<tr>
											  		<td>Tuition Fee</td>
											  		<td><b>&#8369; <?php echo $tuitionfee[$student['level']]; ?></b></td>
											  	</tr>
										    	<?php
										    	$totalfees[] = preg_replace('/[^0-9.]/', '', $tuitionfee[$student['level']]);
		                      $x=0;
		                      foreach (allFees() as $key) {
		                      	if ($key[2]==$student['level']) {
			                        $color = ($x %2 == 1)? 'odd': 'even';
			                        echo '<tr class="'.$color.'">';
			                        echo '<td>'.$key[1].'</td>';
			                        echo '<td><b>&#8369; '.number_format(preg_replace('/[^0-9.]/', '', $key[3]), 2, '.', ',').'</b></td>';
			                        echo '</tr>';
			                        $totalfees[] = $key[3];
			                        $x++;
			                      }
		                      }
		                      ?>
		                      <tr class="success">
											  		<td><b>Total</b></td>
											  		<td><b>&#8369; <?php echo number_format(array_sum($totalfees), 2, '.', ','); ?></b></td>
											  	</tr>
											  </tbody>
											</table>
				            </div>
				          </div>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-12">
									<div class="panel panel-success">
				            <div class="panel-heading">
				              <b>Payment Transactions</b>
				            </div><!-- /.panel-heading -->
				            <div class="panel-body">
				            	<table class="table table-striped table-hover">
											  <thead>
											    <tr>
											      <th>Receipt Number</th>
											      <th>Date of Payment</th>
											      <th>Payment Method</th>
											      <th>Amount Paid</th>
											    </tr>
											  </thead>
											  <tbody>
										    	<?php paymentTransactions($_POST['studentid'], $_POST['schoolyear'], number_format(array_sum($totalfees), 2, '.', ',')); ?>
											  </tbody>
											</table>
				            </div>
				          </div>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">

								</div>
							</div>
							<?php
						} else {
							header("Location: ".get_url()."?error=pass");
	  					exit();
						}
					} else {
						header("Location: ".get_url()."?error=studentid");
	  				exit();
					}
				}	else { ?>

				<form class="form-horizontal" method="post">
				  <fieldset>
				  	<?php if (isset($_GET['error'])) { ?>
				  	<div class="form-group">
							<div class="col-lg-10">
								<div class="alert alert-danger alert-dismissable">
								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  <?php
								  if ($_GET['error']=='pass'){
								  	echo '<b>Error!</b> wrong password combination.';
								  } else {
								  	echo '<b>Error!</b> student ID does not exist.';
								  } ?>
								</div>
							</div>
						</div>
						<?php } ?>
				  	<legend>Inquiry</legend>
				    <div class="form-group<?php echo (isset($_GET['error'])&&$_GET['error']=='studentid')? ' has-error':'';?>">
				      <label for="studentid" class="col-lg-2 control-label">Student ID:</label>
				      <div class="col-lg-10">
				        <input type="text" class="form-control" name="studentid" id="studentid" style="width:50%;">
				      </div>
				    </div>
				    <div class="form-group<?php echo (isset($_GET['error'])&&$_GET['error']=='pass')? ' has-error':'';?>">
				      <label for="studentpass" class="col-lg-2 control-label">Password:</label>
				      <div class="col-lg-10">
				        <input type="password" class="form-control" name="studentpass" id="studentpass" style="width:50%;">
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="select" class="col-lg-2 control-label">School Year</label>
				      <div class="col-lg-10">
				        <select class="form-control" name="schoolyear" id="select" style="width:50%;">
				          <?php
				          foreach (allSchoolyear() as $key => $value) {
				            echo '<option value="'.$value.'">'.$value.'</option>';
				          }
				          ?>
				        </select>
				      </div>
				    </div>
				    <div class="form-group">
				      <div class="col-lg-10 col-lg-offset-2">
				        <button type="submit" class="btn btn-primary">Submit</button>
				      </div>
				    </div>
				  </fieldset>
				</form>
				<?php } ?>
			</div>
		</div>
	</div>

<?php include 'footer.php'; ?>