<?php
require_once('admin/core/connect.php'); // include database connection
require_once('admin/core/user-functions.php'); // include user functions
require_once('admin/core/functions.php'); // include general functions
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>CIC - Colegio De La Inmaculada Concepcion</title>
  <!-- Bootstrap Core CSS -->
  <link href="../css/sandstone.css" rel="stylesheet">
  <!-- Custom Fonts -->
  <link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="navbar-default">
  <div class="container">
    <div class="row">
      <div class="navbar" style="margin-bottom:0;">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php siteurl(); ?>">Colegio De La Inmaculada Concepcion</a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li<?php activeNav('/index.php'); ?>><a href="<?php siteurl(); ?>">Home</a></li>
            <li<?php activeNav('/events.php'); ?>><a href="<?php siteurl(); ?>/events.php">Events</a></li>
            <li<?php activeNav('/staff.php'); ?>><a href="<?php siteurl(); ?>/staff.php">Faculty and Staff</a></li>
            <li<?php activeNav('/inquire.php'); ?>><a href="<?php siteurl(); ?>/inquire.php">Student Inquiry</a></li>
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </li> -->
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>